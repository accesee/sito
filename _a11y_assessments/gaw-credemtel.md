---
color_type: default
ni_icon_name: atom
title: Portale GAW CredemTel
site_url: https://gaw.stg.credemtel.it/gaw/home
summary: 'In collaborazione con <a href="https://sitiaccessibili.it/">SitiAccessibili</a> abbiamo valutato sia il portale di fatturazione elettronica, sia un PDF campione'
date: 2024-10-03
---
---
title: Julian Fashon
color_type: info
ni_icon_name: atom
summary: In collaborazione con <a
  href="https://sitiaccessibili.it/">SitiAccessibili</a> abbiamo valutato
  l'accessibilità del sito di ecommerce Julian Fashon
site_url: https://www.julian-fashion.com/
date: 2024-12-21T17:33:35.090Z
---

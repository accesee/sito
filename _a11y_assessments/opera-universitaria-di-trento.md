---
title: Opera Universitaria di Trento
color_type: info
ni_icon_name: atom
summary: In collaborazione con <a
  href="https://sitiaccessibili.it/">SitiAccessibili</a> abbiamo valutato i siti
  dell'<a href="https://operauni.tn.it/">Opera Universitaria di Trento</a> e del
  portale <a href="https://trent.operauni.tn.it/">TRENT</a> per la ricerca e
  prenotazione degli alloggi
site_url: https://sitiaccessibili.it/
date: 2025-01-26T17:35:56.189Z
---

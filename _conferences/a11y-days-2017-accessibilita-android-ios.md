---
title: Accessibilità nello sviluppo mobile per iOS e Android (Mattia Ducci)
date: 2017-05-20
external_link: https://accessibilitydays.it/2017/
abstract: Questa presentazione intende illustrare i principi e le tecniche per progettare e sviluppare app per iOS e Android in modo che siano accessibili a tutti, anche a coloro che impiegano i lettori vocali e gli strumenti di ingrandimento per utilizzare smartphone e tablet. I principi e le tecniche verranno presentate mediante esempi tratti dall'esperienza acquisita dal laboratorio EveryWare dell'Università degli Studi di Milano nella progettazione e sviluppo di app accessibili.
description: Questa presentazione intende illustrare i principi e le tecniche per progettare e sviluppare app per iOS e Android in modo che siano accessibili a tutti, anche a coloro che impiegano i lettori vocali e gli strumenti di ingrandimento per utilizzare smartphone e tablet. I principi e le tecniche verranno presentate mediante esempi tratti dall'esperienza acquisita dal laboratorio EveryWare dell'Università degli Studi di Milano nella progettazione e sviluppo di app accessibili.
video_embed: '<iframe width="560" height="315" src="https://www.youtube.com/embed/MYmiU9-0UgE?si=PygAYo1348zUNiDW" title="YouTube video player: Accessibilità nello sviluppo mobile per iOS e Android (Mattia Ducci)" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>'
featured_image:
featured_image_alt:
keywords:
---
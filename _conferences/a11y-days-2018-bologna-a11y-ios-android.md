---
title: Accessibilità nello sviluppo mobile per iOS e Android (Mattia Ducci)
date: 2018-05-28
external_link: https://accessibilitydays.it/2018/
abstract: Questa presentazione intende illustrare i principi e le tecniche per progettare e sviluppare app per iOS e Android in modo che siano accessibili a tutti, anche a coloro che impiegano i lettori vocali e gli strumenti di ingrandimento per utilizzare smartphone e tablet. I principi e le tecniche verranno presentate mediante esempi tratti dall'esperienza acquisita dal laboratorio EveryWare dell'Università degli Studi di Milano nella progettazione e sviluppo di app accessibili.
description: Questa presentazione intende illustrare i principi e le tecniche per progettare e sviluppare app per iOS e Android in modo che siano accessibili a tutti, anche a coloro che impiegano i lettori vocali e gli strumenti di ingrandimento per utilizzare smartphone e tablet. I principi e le tecniche verranno presentate mediante esempi tratti dall'esperienza acquisita dal laboratorio EveryWare dell'Università degli Studi di Milano nella progettazione e sviluppo di app accessibili.
video_embed: '<iframe src="https://www.youtube.com/embed/U9KlCUe6Mbs?si=lHzKp1Ws7RpxO3lu" title="YouTube video player: Accessibilità nello sviluppo mobile per iOS e Android (Mattia Ducci)" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>'
keywords: ['a11y', 'mobile development', 'ios', 'android']
---
---
title: Accessibilità su applicazioni mobile cross-platform
date: 2019-05-17
external_link: https://accessibilitydays.it/2019/lectures/mobile-crossplatform/
abstract: >-
    Avete mai provato un'applicazione che avete subito abbandonato perchè inusabile? È un difetto comune a molte applicazioni web, ma non solo... Vi racconteremo di come un approccio ibrido-nativo potrebbe essere la soluzione e di come abbiamo eseguito il porting di un'app iOS in una multi piattaforma mantenendone lo stesso livello di accessibilità usando la tecnologia React Native.
description: >-
    Avete mai provato un'applicazione che avete subito abbandonato perchè inusabile? È un difetto comune a molte applicazioni web, ma non solo... Vi racconteremo di come un approccio ibrido-nativo potrebbe essere la soluzione e di come abbiamo eseguito il porting di un'app iOS in una multi piattaforma mantenendone lo stesso livello di accessibilità usando la tecnologia React Native.
video_embed: '<iframe width="560" height="315" src="https://www.youtube.com/embed/Iq12Rshk9e4?si=ngCt5WXtmeeccwun" title="YouTube video player: Accessibilità su applicazioni mobile cross-platform" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>'
featured_image:
featured_image_alt:
keywords:
---
---
title: Realizzare applicazioni accessibili per dispositivi mobili
date: 2020-05-22
external_link: https://accessibilitydays.it/2020/lectures/mobile-app/
abstract: >-
    Le applicazioni per dispositivi mobili possono essere realizzate o utilizzando i linguaggi per le app native (ad esempio Swift per iOS e Kotlin per Android) o strumenti per creare app multipiattaforma (ad esempio Xamarin). Le tecniche per implementare le caratteristiche di accessibilità delle app variano a seconda dello strumento utilizzato.
    Saranno presentate innanzitutto le caratteristiche di accessibilità che un'applicazione deve avere per poter essere utilizzata da persone con disabilità visiva. Successivamente vengono messe a confronto le principali tecniche per la realizzazione di applicazioni accessibili per dispositivi mobili mediante i linguaggi nativi per iOS e Android e due strumenti per realizzare applicazioni multipiattaforma: Xamarin e React Native.
    La sessione si rivolge ad un pubblico che possiede già buone conoscenze di base di programmazione
description: >-
    Le applicazioni per dispositivi mobili possono essere realizzate o utilizzando i linguaggi per le app native (ad esempio Swift per iOS e Kotlin per Android) o strumenti per creare app multipiattaforma (ad esempio Xamarin). Le tecniche per implementare le caratteristiche di accessibilità delle app variano a seconda dello strumento utilizzato.
    Saranno presentate innanzitutto le caratteristiche di accessibilità che un'applicazione deve avere per poter essere utilizzata da persone con disabilità visiva. Successivamente vengono messe a confronto le principali tecniche per la realizzazione di applicazioni accessibili per dispositivi mobili mediante i linguaggi nativi per iOS e Android e due strumenti per realizzare applicazioni multipiattaforma: Xamarin e React Native.
    La sessione si rivolge ad un pubblico che possiede già buone conoscenze di base di programmazione
video_embed: '<iframe width="560" height="315" src="https://www.youtube.com/embed/3StKRHEiUSU?si=VirD-VoYrk6b6xjM" title="YouTube video player: Realizzare applicazioni accessibili per dispositivi mobili" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>'
featured_image:
featured_image_alt:
keywords:
---
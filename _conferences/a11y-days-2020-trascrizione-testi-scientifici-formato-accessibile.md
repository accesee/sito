---
title: Esperienze di trascrizione di testi scientifici in formati accessibili
date: 2020-05-22
external_link: https://accessibilitydays.it/2020/lectures/trascrizione-testi-scientifici/
abstract: >-
    I testi scientifici pongono vari problemi di accessibilità a persone con disabilità visive. In particolare, le espressioni matematiche devono essere adattate in modo che possano essere lette sul display Braille o dal lettore vocale. Le immagini devono essere descritte o fornite in formati alternativi (ad es. tabelle di valori per i grafici di funzione).
    Il workshop presenta alcune esperienze di trascrizione di testi scientifici in LaTeX. Vengono introdotti sia i flussi di lavoro seguiti sia i principali problemi incontrati.
description: >-
    I testi scientifici pongono vari problemi di accessibilità a persone con disabilità visive. In particolare, le espressioni matematiche devono essere adattate in modo che possano essere lette sul display Braille o dal lettore vocale. Le immagini devono essere descritte o fornite in formati alternativi (ad es. tabelle di valori per i grafici di funzione).
    Il workshop presenta alcune esperienze di trascrizione di testi scientifici in LaTeX. Vengono introdotti sia i flussi di lavoro seguiti sia i principali problemi incontrati.
video_embed: '<iframe width="560" height="315" src="https://www.youtube.com/embed/-z5h4tFDA70?si=AtZScgo9dgKv5ORx" title="YouTube video player: Esperienze di trascrizione di testi scientifici in formati accessibili" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>'
featured_image:
featured_image_alt:
keywords:
---
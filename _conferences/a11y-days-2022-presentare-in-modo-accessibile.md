---
title: Presentare in modo accessibile si può
date: 2022-05-21
external_link: https://accessibilitydays.it/2022/lectures/presentare-accessibile/
abstract: >-
    Creare presentazioni significa sintetizzare e schematizzare e spesso si fa uso di immagini, infografiche, elenchi puntati. Questo lavoro di sintesi fa sì che una presentazione risulti molto utile sia a chi presenta per mantenere un filo logico del discorso, sia a chi la legge senza ascoltare chi la espone per coglierne i punti salienti.
    Esistono molti modi per creare presentazioni: da programmi come Power Point a veri e propri linguaggi di markup come HTML, LaTeX o Markdown.
    Quale scegliere? La risposta è: "dipende", ma ciò che è importante è che anche le presentazioni possono essere accessibili
    In questo intervento rivolto soprattutto a chi crea presentazioni cercheremo di mostrarvi alcuni sistemi per crearle valutandoli rispetto a dei parametri come la facilità di apprendimento, il tempo necessario per creare una presentazione, la possibilità di personalizzazione e ultimo, ma non meno importante, il livello di accessibilità della presentazione risultante da ciascuno
description: >-
    Creare presentazioni significa sintetizzare e schematizzare e spesso si fa uso di immagini, infografiche, elenchi puntati. Questo lavoro di sintesi fa sì che una presentazione risulti molto utile sia a chi presenta per mantenere un filo logico del discorso, sia a chi la legge senza ascoltare chi la espone per coglierne i punti salienti.
    Esistono molti modi per creare presentazioni: da programmi come Power Point a veri e propri linguaggi di markup come HTML, LaTeX o Markdown.
    Quale scegliere? La risposta è: "dipende", ma ciò che è importante è che anche le presentazioni possono essere accessibili
    In questo intervento rivolto soprattutto a chi crea presentazioni cercheremo di mostrarvi alcuni sistemi per crearle valutandoli rispetto a dei parametri come la facilità di apprendimento, il tempo necessario per creare una presentazione, la possibilità di personalizzazione e ultimo, ma non meno importante, il livello di accessibilità della presentazione risultante da ciascuno
video_embed: '<iframe width="560" height="315" src="https://www.youtube.com/embed/9MrW3wmGk98?si=UdIj7OnlDXZAd6Hw" title="YouTube video player: Presentare in modo accessibile si può" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>'
featured_image:
featured_image_alt:
keywords:
---
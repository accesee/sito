---
title: '"SpeechMate": un sistema per la dettatura della matematica'
date: 2023-05-18
external_link: https://accessibilitydays.it/2023/sessione/speechmate-dettatura-matematica/
abstract: >-
    Le persone con disabilità o disordini motori agli arti superiori (come diplegia, tetraplegia, disgrafia e disprassia) incontrano numerose difficoltà nella scrittura della matematica sia su carta sia al computer. Tali difficoltà impediscono l’elaborazione autonoma di espressioni matematiche, abilità indispensabile negli studi scientifici.
    Questo intervento presenta il progetto SpeechMate del laboratorio Polin dell’Università degli Studi di Torino che permette la dettatura di espressioni matematiche in lingua italiana. Dopo una prima spiegazione sul suo funzionamento, ci concentreremo sulle potenzialità di sue possibili evoluzioni, sui risultati e sui riconoscimenti che finora ha ottenuto.
description: >-
    Le persone con disabilità o disordini motori agli arti superiori (come diplegia, tetraplegia, disgrafia e disprassia) incontrano numerose difficoltà nella scrittura della matematica sia su carta sia al computer. Tali difficoltà impediscono l’elaborazione autonoma di espressioni matematiche, abilità indispensabile negli studi scientifici.
    Questo intervento presenta il progetto SpeechMate del laboratorio Polin dell’Università degli Studi di Torino che permette la dettatura di espressioni matematiche in lingua italiana. Dopo una prima spiegazione sul suo funzionamento, ci concentreremo sulle potenzialità di sue possibili evoluzioni, sui risultati e sui riconoscimenti che finora ha ottenuto.
video_embed: '<iframe width="560" height="315" src="https://www.youtube.com/embed/sHqbq_lh03o?si=SCmixzH8VAbMt2KE" title="YouTube video player: SpeechMate: un sistema per la dettatura della matematica" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>'
featured_image:
featured_image_alt:
keywords:
---
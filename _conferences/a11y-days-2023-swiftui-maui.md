---
title: "Accessibilità su dispositivi mobili su due framework moderni: SwiftUI e .NET MAUI - Accessibility days (Roma)"
date: 2023-05-19
external_link: https://accessibilitydays.it/2023/sessione/accessibilita-mobile-swiftui-net-maui/
abstract: >-
    Il mondo dello sviluppo mobile diventa ogni anno sempre più frammentato grazie a framework di sviluppo che vanno incontro a programmatori provenienti dalle più diverse esperienze di sviluppo. Tutti questi framework hanno però uno stesso punto di arrivo: creare applicazioni iOS o Android. Sono loro infatti ad offrire le caratteristiche di accessibilità che l’utente finale percepisce e un buon framework accessibile è quello che offre un’interfaccia chiara e completa a queste caratteristiche.
    Negli anni il progresso in direzione dell’accessibilità non si è mai arrestato, ma a seconda dei framework si possono notare differenze in termini di funzionalità, semplicità di utilizzo e di capacità di adattarsi ai cambiamenti che iOS e Android hanno introdotto.
    Oggi vediamo come hanno inteso il progresso due tra i più moderni framework: SwiftUI e .NET MAUI. Il primo, nativo iOS, si offre come successore della decennale libreria UIKit; il secondo, cross-platform, come successore di Xamarin
description: >-
    Il mondo dello sviluppo mobile diventa ogni anno sempre più frammentato grazie a framework di sviluppo che vanno incontro a programmatori provenienti dalle più diverse esperienze di sviluppo. Tutti questi framework hanno però uno stesso punto di arrivo: creare applicazioni iOS o Android. Sono loro infatti ad offrire le caratteristiche di accessibilità che l’utente finale percepisce e un buon framework accessibile è quello che offre un’interfaccia chiara e completa a queste caratteristiche.
    Negli anni il progresso in direzione dell’accessibilità non si è mai arrestato, ma a seconda dei framework si possono notare differenze in termini di funzionalità, semplicità di utilizzo e di capacità di adattarsi ai cambiamenti che iOS e Android hanno introdotto.
    Oggi vediamo come hanno inteso il progresso due tra i più moderni framework: SwiftUI e .NET MAUI. Il primo, nativo iOS, si offre come successore della decennale libreria UIKit; il secondo, cross-platform, come successore di Xamarin
video_embed:
featured_image: /assets/img/conferences/a11y-days-2023-maui-swiftui.jpg
featured_image_alt: Immagine di copertina del workshop che mostra il titolo del webinar e presenta il relatore, Mattia, come sviluppatore freelance web e mobile
keywords:
---
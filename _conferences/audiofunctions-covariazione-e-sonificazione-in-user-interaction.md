---
title: " Audiofunctions, covariazione e sonificazione in user interaction"
date: 2025-02-18T08:05:31.534Z
external_link: https://www.sema.unito.it/
description: Intervento sugli avanzamenti Audiofunctions con particolare focus
  sull'interazione dell'utente
abstract: Nella seconda edizione di SeMa si è scelto di mettere il focus sul
  confronto interdisciplinare che esiste tra i gruppi di lavoro di didattica
  della matematica e quelli informatici. Abbiamo confrontato terminologie e
  metodi rendendoci conto che ci sono casi in cui termini uguali intendono
  significati diversi. Per creare prodotti digitali didattici è fondamentale
  collaborare tra questi due mondi per creare realmente un software accessibile
  e con solide basi e intenzionalità didattiche. Con questo spirito con Silvia
  Funghi siamo partiti dallo stato di Audiofunctions dell'anno scorso
  aggiungendo gli avanzamenti fatti in termini implementativi e di test con gli
  utenti
featured_image: /assets/uploads/sema.png
featured_image_alt: logo sema
---

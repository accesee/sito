---
title: .NET in pausa caffè - Accessibilità del software
date: 2021-12-23
external_link: https://www.youtube.com/watch?v=rULhDWDYBEk
abstract: >-
    Dot Net in pausa caffè
    In questa serie parleremo di accessibilità del software, ovvero come rendere il nostro software veramente utilizzabile da tutti.
    Scopriremo come lo scrivere software accessibile permetta anche indubbi vantaggi competitivi. La lista
    completa di video che fanno parte di questa serie di webinar è disponibile nella pagina di dettaglio
description: >-
    Dot Net in pausa caffè
    In questa serie parleremo di accessibilità del software, ovvero come rendere il nostro software veramente utilizzabile da tutti.
    Scopriremo come lo scrivere software accessibile permetta anche indubbi vantaggi competitivi. La lista
    completa di video che fanno parte di questa serie di webinar è disponibile nella pagina di dettaglio
video_embed: '<iframe src="https://www.youtube.com/embed/rULhDWDYBEk?si=Cbp1UT_uBxjqiZiG" title="YouTube video player: .NET in pausa caffè - Accessibilità del software" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>'
featured_image:
featured_image_alt:
keywords: ['a11y','software']
---
## Lista completa dei video

### Parte 1

<iframe width="560" height="315" src="https://www.youtube.com/embed/rULhDWDYBEk?si=qeq8bKv3VwUkGkz_" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

### Parte 2

<iframe width="560" height="315" src="https://www.youtube.com/embed/a3TxNr3epH0?si=0K9RvosqxcXn8wdE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

### Parte 3

<iframe width="560" height="315" src="https://www.youtube.com/embed/YyjrQ9xh3jQ?si=3d_WCoIbv182C3j5" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

### Parte 4

<iframe width="560" height="315" src="https://www.youtube.com/embed/86VazWEy0kw?si=-_K7KDnwgHn62zsx" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

### Parte 5

<iframe width="560" height="315" src="https://www.youtube.com/embed/LQDpc_9RfRY?si=38_W83qiV3Gh_Ak0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

### Parte 6

<iframe width="560" height="315" src="https://www.youtube.com/embed/8BIbFQ-q-LI?si=Q-JAspgR_jPkBpWB" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
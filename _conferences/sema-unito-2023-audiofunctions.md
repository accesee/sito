---
title: "Audiofunctions: a software for graphs sonification"
date: 2023-12-05
external_link: https://www.sema.unito.it/home-page
abstract: >-
    Lo scopo di questa presentazione è stato quello di presentare alla prima conferenza
    SeMa (Sensing Mathematics) il software Audiofunctions 2.0, un software inclusivo per la sonificazione
    di funzioni matematiche ad una variabile <i>y=f(x)</i>. Nel corso della conferenza
    abbiamo raccolto feedback da insegnanti e ricercatori nel campo della didattica della
    matematiche per capire come migliorare il software e renderlo più accessibile e utile
    alle esigenze didattiche
description: >-
    Lo scopo di questa presentazione è stato quello di presentare alla prima conferenza
    SeMa (Sensing Mathematics) il software Audiofunctions 2.0, un software inclusivo per la sonificazione
    di funzioni matematiche ad una variabile <i>y=f(x)</i>. Nel corso della conferenza
    abbiamo raccolto feedback da insegnanti e ricercatori nel campo della didattica della
    matematiche per capire come migliorare il software e renderlo più accessibile e utile
    alle esigenze didattiche
video_embed:
featured_image: /assets/img/conferences/sema.png
featured_image_alt: Logo della conferenza SeMa
---
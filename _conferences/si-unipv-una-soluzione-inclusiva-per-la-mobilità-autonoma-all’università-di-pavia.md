---
title: "SI@UNIPV: una soluzione inclusiva per la mobilità autonoma
  all’Università di Pavia"
date: 2024-05-17T14:37:56.393Z
external_link: https://accessibilitydays.it/2024/sessione/mobilita-autonoma-unipv/
description: >-
  Le persone con disabilità incontrano molte difficoltà nella mobilità e
  orientamento all’interno di edifici come università, centri commerciali e
  ospedali. Mentre all’esterno sono accessibili servizi basati su GPS (es Google
  Maps ), all’interno questi servizi non sono disponibili o affidabili.
  L’Università di Pavia nell’ambito del progetto RISID ha studiato e sviluppato
  una soluzione per favorire la mobilità autonoma di tutti, incluse le persone
  con disabilità, all’interno dell’Ateneo. La soluzione si basa su una rete di
  beacon installati all’interno dell’Università e su un’applicazione iOS che
  guida le persone all’interno degli spazi universitari: per esempio per
  raggiungere un’aula, un’uscita o i servizi igienici. Inoltre l’applicazione
  consente di scoprire luoghi di interesse culturale attraverso percorsi
  tematici.


  La sessione presenta l’esperienza di progettazione e sviluppo dell’applicazione SI@UNIPV in riferimento ai problemi di accessibilità e le prime esperienze di utilizzo.
abstract: >-
  Le persone con disabilità incontrano molte difficoltà nella mobilità e
  orientamento all’interno di edifici come università, centri commerciali e
  ospedali. Mentre all’esterno sono accessibili servizi basati su GPS (es Google
  Maps ), all’interno questi servizi non sono disponibili o affidabili.
  L’Università di Pavia nell’ambito del progetto RISID ha studiato e sviluppato
  una soluzione per favorire la mobilità autonoma di tutti, incluse le persone
  con disabilità, all’interno dell’Ateneo. La soluzione si basa su una rete di
  beacon installati all’interno dell’Università e su un’applicazione iOS che
  guida le persone all’interno degli spazi universitari: per esempio per
  raggiungere un’aula, un’uscita o i servizi igienici. Inoltre l’applicazione
  consente di scoprire luoghi di interesse culturale attraverso percorsi
  tematici.


  La sessione presenta l’esperienza di progettazione e sviluppo dell’applicazione SI@UNIPV in riferimento ai problemi di accessibilità e le prime esperienze di utilizzo.
video_embed: <iframe width="560" height="315"
  src="https://www.youtube.com/embed/XcHiPDwGNK4?si=yu05l684A-4gbPmA"
  title="YouTube video player" frameborder="0" allow="accelerometer; autoplay;
  clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
  referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
---

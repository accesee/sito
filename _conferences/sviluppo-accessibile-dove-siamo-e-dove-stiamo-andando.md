---
title: "Sviluppo accessibile: dove siamo e dove stiamo andando"
date: 2024-05-17T14:36:20.675Z
external_link: https://accessibilitydays.it/2024/sessione/sviluppo-accessibile-roundtable/
description: >-
  In questa tavola rotonda si affronteranno temi di maggior rilievo legati alle
  sfide di uno sviluppo orientato all’accessibilità.

  L’evoluzione dei paradigmi di sviluppo e di interazione hanno modificato anche le modalità necessarie per integrare le caratteristiche di accessibilità, che però spesso rischiano di rimanere incomplete e talvolta anche dimenticate.

  In questo confronto si cercherà di tracciare un quadro delle attuali strategie per l’accessibilità, sia in ambiente desktop che mobile, e di analizzare cosa ci aspetta nel futuro.
abstract: >-
  In questa tavola rotonda si affronteranno temi di maggior rilievo legati alle
  sfide di uno sviluppo orientato all’accessibilità.

  L’evoluzione dei paradigmi di sviluppo e di interazione hanno modificato anche le modalità necessarie per integrare le caratteristiche di accessibilità, che però spesso rischiano di rimanere incomplete e talvolta anche dimenticate.

  In questo confronto si cercherà di tracciare un quadro delle attuali strategie per l’accessibilità, sia in ambiente desktop che mobile, e di analizzare cosa ci aspetta nel futuro.
video_embed: <iframe width="560" height="315"
  src="https://www.youtube.com/embed/vjEOEPwIL4A?si=p0PzNeX-UZADgGNs"
  title="YouTube video player" frameborder="0" allow="accelerometer; autoplay;
  clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
  referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
---

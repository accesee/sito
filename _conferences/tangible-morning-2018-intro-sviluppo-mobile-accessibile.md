---
title: Sviluppare applicazioni mobile accessibili. Come? - Tangible Morning, S.Arcangelo di Romagna(RN)
date: 2018-11-22
external_link: https://tangible.is/inspiration/inspired-by-unioneciechiancona_and_universalaccess
abstract: >-
    Mattinata in collaborazione con l'UICI di Ancona e Universal Access per parlare di tecnologie assistive a Tangible, una realtà di designer e sviluppatori. Il mio intervento si è focalizzato sul mostrare praticamente a livello di codice che strumenti base si hanno a disposizione per iniziare a creare un'applicazione iOS o Android accessibile
description: >-
    Mattinata in collaborazione con l'UICI di Ancona e Universal Access per parlare di tecnologie assistive a Tangible, una realtà di designer e sviluppatori. Il mio intervento si è focalizzato sul mostrare praticamente a livello di codice che strumenti base si hanno a disposizione per iniziare a creare un'applicazione iOS o Android accessibile
video_embed: <iframe title="Sviluppare applicazioni mobile accessibili. Come? - Tangible Morning, S.Arcangelo di Romagna(RN)" src="https://www.facebook.com/plugins/video.php?height=314&href=https%3A%2F%2Fwww.facebook.com%2Fwethinktangible%2Fvideos%2F508693622977614%2F&show_text=false&width=560&t=0" width="560" height="314" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share" allowFullScreen="true"></iframe>
featured_image:
featured_image_alt:
keywords:
---
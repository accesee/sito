---
name: Università degli Studi di Milano
logo: /assets/img/customers/LogoUNIMI.png
website: https://di.unimi.it/it
order: 7
---
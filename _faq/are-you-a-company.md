---
question: Siete una società?
answer: >-
    No, Accesee è un progetto astratto realizzato da molte persone e aziende per un fine comune.
    Accesee per ora non è quindi una società, ma io, Mattia che insieme ad altre persone e ad altre aziende
    con le quali collaboro, cerco di mettere sotto questo nome un ideale e una serie di progetti che
    possano a poco a poco tendere verso un obiettivo di accessibilità il più ampio possibile.
---
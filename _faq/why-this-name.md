---
question: Da dove nasce il nome "Accesee"
answer: >-
    Il nome "Accesee" è composto principalmente da due parole: "Accessibilità" e "Visione" ("see" in inglese).
    Se unite queste due parole formano "Accesee" che in lingua italiana assomiglia molto alla parola "accesi".
    Con riferimento ai colori, alla luce e alla trasparenza che vorrei che questo progetto trasmettesse mi collego
    ad una frase di una canzone di Fabrizio Moro: <i>"Non importa se sei bianco o se sei nero, se sei falso o se sei vero, 
    se sei forte o sei indifeso, ogni colore alla luce è più acceso"</i>
---
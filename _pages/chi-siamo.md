---
title: Chi siamo
subtitle: "Ecco il cuore di Accesee: un team di professionisti appassionati che rende possibile ogni nostra innovazione"
description: Visita la pagina 'Chi Siamo' di Accesee per conoscere i membri del nostro team e scoprire i loro ruoli e specializzazioni. Ogni membro è descritto attraverso keywords che riflettono la loro identità professionale e il loro contributo unico alla missione di rendere il mondo più accessibile. Scopri gli esperti che guidano la nostra innovazione e il nostro impegno verso l'accessibilità.
featured_image: /assets/img/sections/chi-siamo-bg.jpg
---

---
### Chi è Accesee?

Accesee è un progetto realizzato da molte persone e aziende per un fine comune.
    Accesee per ora non è quindi una società, ma sono io, **Mattia** che insieme ad altre persone, aziende e associazioni
    cerco di mettere sotto questo nome un ideale e una serie di progetti che
    possano a poco a poco tendere verso un obiettivo di accessibilità il più ampio possibile.


{% include components/teams/team.html %}

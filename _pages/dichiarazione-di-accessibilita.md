---
layout: default
title: Dichiarazione di accessibilità
---
Data di pubblicazione della dichiarazione: 09/04/2024

Noi di Accesee ci impegniamo a rendere accessibile il nostro sito web a tutti gli utenti, inclusi coloro con disabilità. Questa dichiarazione di accessibilità riassume i nostri sforzi per garantire che il nostro sito web sia conforme alle Linee Guida per l'Accessibilità dei Contenuti Web, Versione 2.1 livello AA (WCAG 2.1 AA).

### Conformità
Il sito web [inserire il nome del sito web] si impegna a garantire che la sua piattaforma digitale sia conforme alle WCAG 2.1 livello AA. Abbiamo condotto un'attenta revisione del nostro sito e abbiamo implementato modifiche per assicurarci che tutte le pagine del sito web siano accessibili a persone con varie disabilità.

### Feedback
La vostra opinione è fondamentale per noi. Se incontrate qualsiasi problema durante l'utilizzo del nostro sito web o avete suggerimenti su come possiamo migliorare l'accessibilità, vi preghiamo di contattarci utilizzando uno dei seguenti metodi:

- Email: [info@accesee.it](mailto:info@accesee.it)
- Telefono: +393803423230 (risponderà Mattia Ducci)
- Indirizzo: via Giovanni Falcone, 3, 25032, Chiari (BS)

Ci impegniamo a rispondere a feedback e richieste di assistenza il più rapidamente possibile.

### Misure Adottate per Supportare l'Accessibilità
Accesee prende le seguenti misure per garantire l'accessibilità:

- Integrazione delle pratiche di accessibilità nelle nostre politiche interne.
- Assegnazione di risorse specifiche per garantire l'accessibilità del nostro sito web.
- Formazione continua del nostro personale sull'accessibilità.
- Revisione periodica del sito web da parte di esperti interni ed esterni.
- Chiaro impegno a risolvere rapidamente le questioni di accessibilità identificate.

### Tecnologie Supportate
Il nostro sito web mira a supportare una vasta gamma di tecnologie assistive e browser. Tuttavia, la varietà di tecnologie può comportare alcune incompatibilità. Se riscontrate difficoltà con una specifica combinazione di dispositivo/tecnologia, vi preghiamo di segnalarcelo.

### Accessibilità delle Funzioni Interattive
Ci impegniamo a rendere tutte le funzioni interattive del nostro sito accessibili a persone con disabilità. Ogni nuova funzionalità verrà testata per garantire la conformità con le WCAG 2.1 AA prima del suo rilascio.

### Stato Attuale e Futuri Miglioramenti
Accesee continua a lavorare per migliorare l'accessibilità del sito web. Riconosciamo che non tutte le aree potrebbero essere completamente accessibili e stiamo pianificando miglioramenti futuri per continuare il nostro impegno verso l'accessibilità.

### Aggiornamenti della Dichiarazione
Questa dichiarazione è stata ultimata il 09/04/2024 e verrà rivista e aggiornata annualmente o ogni volta che verranno implementate modifiche significative.
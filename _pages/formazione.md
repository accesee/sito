---
title: Formazione
subtitle: Formare insegna sia a chi insegna che a chi impara
description: Uno degli obiettivi principali di Accesee è contribuire a creare una cultura dell'accessibilità e non c'è niente di meglio della formazione per comunicare questo mondo a realtà diverse.
featured_image: /assets/img/sections/unsplashs.jpg
---

---
### Formazione generale e su misura sui temi dell'accessibilità

In collaborazione con [SitiAccessibili](https://sitiaccessibili.it/) abbiamo intrapreso un percorso di formazione e divulgazione sui temi dell'accessibilità digitale. La formazione è un aspetto fondamentale per creare una cultura dell'accessibilità e per questo motivo la applichiamo a contesti privati e pubblici su vari temi legati all'accessibilità: dalle basi all'adattamento personalizzato al flusso di lavoro e ai materiali dell'azienda.


<ul aria-label="Attività di formazione" style="padding-inline-start: unset;">
    {% assign sorted_teachings = site.teaching | sort: "end_date" | reverse %}
        {% for teaching in sorted_teachings %}
            <li style="list-style-type: none;">
                {%- include components/teachings/teaching.html teaching=teaching -%}
            </li>
            
        {% endfor %}
</ul>

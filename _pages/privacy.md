---
layout: default
title: Privacy
subtitle: Per noi la tua privacy è importante
---
### Informativa sulla Privacy

Data di ultimo aggiornamento: 14/11/2024

#### Informativa ai sensi dell’art. 13 del Regolamento europeo 679/2016 e consenso

Ai sensi dell’art. 13 del Regolamento europeo (UE) 2016/679 (di seguito GDPR), e in relazione ai dati personali di cui il consulente entrerà nella disponibilità con l’affidamento della Sua adesione/richiesta del servizio, anche inerente il periodo di prova, Le comunico quanto segue:

#### Titolare del trattamento e responsabile della protezione dei dati personali

Titolare del trattamento è la ditta Accesee in persona di Mattia Ducci (di seguito indicato anche come "consulente") con domicilio eletto in Via G. Falcone, n 3 - Chiari (BS). Il Titolare può essere contattato mediante e mail ordinaria all'indirizzo [info@accesee.it](mailto:info@accesee.it) o PEC mattia.ducci@pec.it o mediante lettera inviata al domicilio sopra indicato. Il Titolare non ha nominato un responsabile della protezione dei dati personali.

#### Finalità del trattamento dei dati

Il trattamento è finalizzato alla corretta e completa esecuzione dell’incarico professionale ricevuto. I suoi dati saranno trattati anche al fine di:

- adempiere agli obblighi previsti in ambito fiscale e contabile
- rispettare gli obblighi incombenti sul consulente e previsti dalla normativa vigente

I dati personali potranno essere trattati a mezzo sia di archivi cartacei che informatici (ivi compresi dispositivi portatili) e trattati con modalità strettamente necessarie a far fronte alle finalità sopra indicate.

#### Base giuridica del trattamento

Il consulente tratta i Suoi dati personali lecitamente, laddove il trattamento:

- sia necessario all’esecuzione del mandato, di un contratto di cui Lei è parte o all’esecuzione di misure precontrattuali adottate su richiesta;
- sia necessario per adempiere un obbligo legale incombente sul consulente
- sia basato sul consenso espresso: ricevere aggiornamenti sullo stato del servizio o comunicazioni inerenti lo stesso o eventuali novità sull'indirizzo e mail fornito al momento dell’adesione al servizio, anche per quanto concerne l’eventuale periodo di prova

#### Conseguenze della mancata comunicazione dei dati personali

Con riguardo ai dati personali relativi all'esecuzione del contratto di cui Lei è parte o relativi all'adempimento ad un obbligo normativo (ad esempio gli adempimenti legati alla tenuta delle scritture contabili e fiscali), la mancata comunicazione dei dati personali impedisce il perfezionarsi del rapporto contrattuale stesso.

#### Conservazione dei dati

I Suoi dati personali, oggetto di trattamento per le finalità sopra indicate, saranno conservati per il periodo di durata del contratto e, successivamente, per il tempo in cui il consulente sia soggetto a obblighi di conservazione per finalità fiscali o per altre finalità, previsti, da norme di legge o regolamento. Negli altri casi i dati saranno cancellati nel più breve tempo possibile e comunque non oltre sei (6) mesi dalla raccolta o dalla conclusione del servizio.

#### Comunicazione dei dati

I Suoi dati personali potranno essere comunicati a:

- consulenti e commercialisti o altri consulenti che eroghino prestazioni funzionali ai fini sopra indicati;
- istituti bancari e assicurativi che eroghino prestazioni funzionali ai fini sopra indicati
- soggetti che elaborano i dati in esecuzione di specifici obblighi di legge
- eventuali autorità giudiziarie o amministrative, per l’adempimento degli obblighi di legge

#### Profilazione e Diffusione dei dati

I Suoi dati personali non sono soggetti a diffusione né ad alcun processo decisionale interamente automatizzato, ivi compresa la profilazione.

#### Diritti dell’interessato

Tra i diritti a Lei riconosciuti dal GDPR rientrano quelli di:

- chiedere al consulente **l'accesso** ai Suoi dati personali ed alle informazioni relative agli stessi; la rettifica dei dati inesatti o l'integrazione di quelli incompleti; la cancellazione dei dati personali che La riguardano (al verificarsi di una delle condizioni indicate nell'art. 17, paragrafo 1 del GDPR e nel rispetto delle eccezioni previste nel paragrafo 3 dello stesso articolo); la limitazione del trattamento dei Suoi dati personali (al ricorrere di una delle ipotesi indicate nell'art. 18, paragrafo 1 del GDPR);
- richiedere ed ottenere dal consulente - nelle ipotesi in cui la base giuridica del trattamento sia il contratto o il consenso, e lo stesso sia effettuato con mezzi automatizzati, che i Suoi dati personali in un formato strutturato e leggibile da dispositivo automatico, siano **trasferiti** ad un altro titolare del trattamento (c.d. diritto alla portabilità dei dati personali);
- **opporsi** in qualsiasi momento al trattamento dei Suoi dati personali al ricorrere di situazioni particolari che La riguardano;
- **revocare** il consenso in qualsiasi momento, limitatamente alle ipotesi in cui il trattamento sia basato sul Suo consenso per una o più specifiche finalità e riguardi dati personali comuni (ad esempio data e luogo di nascita o luogo di residenza), oppure particolari categorie di dati (ad esempio dati che rivelano la Sua origine razziale, le Sue opinioni politiche, le Sue convinzioni religiose, lo stato di salute o la vita sessuale). Il trattamento basato sul consenso ed effettuato antecedentemente alla revoca dello stesso conserva, comunque, la sua liceità;
- **proporre reclamo** a un'autorità di controllo (Autorità Garante per la protezione dei dati personali – www.garanteprivacy.it).

---
title: Prodotti
subtitle: I risultati della nostra attività di sviluppo
description: Scopri i prodotti che Accesee ha sviluppato e mette a tua disposizione
featured_image: /assets/img/sections/unsplashs.jpg
---

---
### In Accesee accessibilità, ricerca e sviluppo si fondono per creare prodotti innovativi e inclusivi.

I prodotti che sviluppiamo sono il risultato di un lavoro di ricerca e sviluppo che mette al centro l'accessibilità e l'inclusione.
La nostra filosofia è quella di creare prodotti che risolvano singoli problemi puntuali nel modo più immediato e intuitivo possibile per l'utente.
> Less is more

<ul aria-label="prodotti" style="padding-inline-start: unset;">
        {% for product in site.products %}
            <li style="list-style-type: none;">
                {%- include components/products/product.html product=product -%}
            </li>
            
        {% endfor %}
</ul>

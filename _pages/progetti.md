---
title: Progetti
subtitle: Lavori ai quali abbiamo preso parte o che ci sono stati affidati.
description: Scopri i progetti innovativi di Accesee in cui abbiamo apportato un contributo significativo. Questa pagina mette in luce come la nostra expertise in accessibilità e tecnologia trasformi idee in soluzioni concrete, migliorando l'accesso e l'usabilità in vari settori. Ogni progetto dimostra il nostro impegno a creare un mondo più inclusivo e accessibile.
featured_image: /assets/img/sections/unsplashs.jpg
---

---
### In fondo che cos'è l'accessibilità se non qualunque cosa progettata attorno alla persona?

Scopri di più riguardo ai progetti ai quali abbiamo preso parte: ciascuno un tassello fondamentale nel mosaico della nostra missione per un futuro accessibile e inclusivo. Qui, la visione di Accesee prende forma attraverso soluzioni concrete che abbattano le barriere e aprano nuove strade verso l'innovazione e l'inclusione per tutti.
{% assign ongoing_projects = site.projects | where: "in_progress", true %}
{% assign sorted_ongoing_projects = ongoing_projects | sort: "year" | reverse %}
{% assign ended_projects = site.projects | where: "in_progress", false %}
{% assign sorted_ended_projects = ended_projects | sort: "year" | reverse %}
<ul aria-label="progetti" style="padding-inline-start: unset;">
    
        {% for project in sorted_ongoing_projects %}
            <li style="list-style-type: none;">
                {%- include components/projects/project.html project=project -%}
            </li>
        {% endfor %}
        {% for project in sorted_ended_projects %}
            <li style="list-style-type: none;">
                {%- include components/projects/project.html project=project -%}
            </li>
            
        {% endfor %}
        {% comment %} {% for project in site.projects %}
            <li style="list-style-type: none;">
                {%- include components/projects/project.html project=project -%}
            </li>
            
        {% endfor %} {% endcomment %}
</ul>

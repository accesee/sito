---
title: Il progetto Accesee
subtitle: Risolvere i problemi di molti migliorando la vita di tutti
description: >-
    Accesee, fondata nel 2019 da Mattia, si impegna a rendere l'accessibilità la norma, non l'eccezione. Collaborando con le università di Torino, Milano e Pavia, Accesee sviluppa soluzioni che trasformano l'accesso a scienza e tecnologia, eliminando barriere visive, motorie e cognitive. Dal 2023, con l'impegno a tempo pieno di Mattia e il rafforzamento delle collaborazioni, la nostra missione è espandere l'offerta, dimostrando che l'accessibilità è un valore aggiunto essenziale nel business digitale.
featured_image: /assets/img/sections/unsplashs.jpg
---

---

## La storia di un'idea
Il viaggio di Accesee inizia nel 2019, alimentato dalla visione di Mattia: creare un ecosistema dove i servizi accessibili non siano un'eccezione, ma la norma.

Mattia, fin dall'inizio, ha creduto fermamente nell'importanza dell'accessibilità. La collaborazione con le università di Torino e Milano 
ha segnato i primi passi concreti verso la realizzazione di questo sogno. Queste prime esperienze non sono state solo occasioni di apprendimento, ma anche il terreno fertile da cui è cresciuto il nostro impegno nel rendere la scienza e la tecnologia accessibili a tutti, superando barriere visive, motorie e cognitive.
Dalla trascrizione di documenti scientifici ad applicazioni web e mobile accessibili i progetti si sono accumulati, e con essi la consapevolezza che l'accessibilità non è solo un obbligo, ma un'opportunità per creare un mondo migliore.

## Crescere nel Business dell'Accessibilità
Grazie all'impegno costante e alla collaborazione stretta con il mondo accademico, inclusa l'Università di Pavia, Accesee ha iniziato a 
raccogliere i primi frutti significativi. I nostri servizi, che spaziano dalla valutazione dell'accessibilità 
dei siti web allo sviluppo di applicazioni per dispositivi mobili, non solo migliorano la vita di molti, 
ma aprono anche nuove porte nel mercato, evidenziando come l'accessibilità possa essere un fattore chiave di 
differenziazione e crescita nel business digitale.
La vera svolta è arrivata nel 2023 quando Mattia ha deciso di dedicarsi a tempo pieno 
all'accessibilità permettendo il rafforzamento della collaborazione con Cristian, 
con le Università e aprendo le porte anche a nuovi clienti
trasformando la passione in una strategia imprenditoriale. 
La nostra mission? Creare soluzioni accessibili che non solo rispondano a un bisogno sociale, 
ma che si rivelino anche vantaggiose per il business.


## Il Futuro è Accesseebile
Il viaggio di Accesee dimostra come un approccio etico e orientato all'accessibilità possa essere trasformato 
in una solida strategia di business. Guardiamo al futuro con l'obiettivo di espandere 
ulteriormente la nostra offerta, rendendo l'accessibilità un valore aggiunto riconosciuto e ricercato nel panorama digitale.

---







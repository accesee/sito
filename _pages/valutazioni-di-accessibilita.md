---
title: Valutazioni di accessibilità
subtitle: Siti web, applicazioni e documenti che abbiamo valutato dal punto di vista dell'accessibilità
description: Scopri a quali valutazioni di accessibilità di siti web, applicazioni e documenti abbiamo partecipato
featured_image: /assets/img/sections/unsplashs.jpg
---

---
### Valutare il materiale esistente è il primo passo per capire dove intervenire

Le valutazioni di accessibilità sono un'attività fondamentale per capire il livello di accessibilità di un sito web, di un'applicazione o di un documento esistenti.
Il nostro metodo di lavoro si basa su una combinazione di test automatici e manuali, condotti anche con tecnologie assistive e persone con disabilità.
A seconda del sito e delle esigenze del cliente, possiamo fare una valutazione completa o parziale, con un focus su particolari flussi di lavoro (ad esempio una procedura di acquisto o di autenticazione) o pagine specifiche ritenute importanti.
Dal momento poi che la maggior parte dei siti web è composta da componenti che si ripetono in più parti del sito, offriamo un preventivo gratuito per valutare l'entità del lavoro necessario cercando di identificare le pagine più rappresentative.
Di seguito sono elencate le valutazioni di accessibilità a cui abbiamo partecipato con attività di report and repair.

<ul aria-label="valutazioni di accessibilità" style="padding-inline-start: unset;">
    {% assign sorted_a11y_assessment = site.a11y_assessments | sort: "date" | reverse %}
        {% for assessment in sorted_a11y_assessment %}
            <li style="list-style-type: none;">
                {%- include components/a11y_assessments/a11y_assessment.html assessment=assessment -%}
            </li>
            
        {% endfor %}
</ul>

---
title: Saremo in presenza agli Accessibility days 2024 a Roma il 16 e 17 maggio
date: 2024-04-23 00:00:00
description: Al consueto appuntamento degli Accessibility days quest'anno parleremo dell'esperienza che abbiamo avuto con l'Università di Pavia nell'ambito del progetto RISID. Avremo poi un tavolo di confronto con esperti di accessibilità per fare un punto sulla situazione dell'accessibilità in Italia.
featured_image: /assets/img/a11ydays-logo.png
featured_image_alt: Logo degli accessibility days
author: Mattia
---

Quest'anno avremo due appuntamenti, tutti di venerdì:

* Il primo appuntamento dalle 12:00 alle 13:00 è dedicato ad un'analisi riguardo alla situazione dello sviluppo accessibile e i suoi possibili sviluppi.
A questo incontro parteciparanno anche esperti del settore come Cristian Bernareggi, Sauro Cesaretti, Diana Bernabei e Fabrizio Caccavello. 
Potete trovare i [dettagli dell'incontro](https://accessibilitydays.it/2024/sessione/sviluppo-accessibile-roundtable/).

* Il secondo dalle 14:30 alle 15:15 dedicato alla presentazione del progetto RISID. In questo progetto
Mattia e Cristian Bernareggi hanno collaborato con l'Università di Pavia per migliorare la mobilità autonoma all'interno
degli spazi del palazzo centrale dell'Università di Pavia creando un'app iOS accessibile e sfruttando dei beacon bluetooth per
rendere possibile una localizzazione efficace e precisa anche indoor. Potete trovare tutti i [dettagli dell'incontro](https://accessibilitydays.it/2024/sessione/mobilita-autonoma-unipv/).
---
title: La magia degli Accessibility days
date: 2024-08-03T13:40:02.534Z
description: Gli Accessibility days non sono solo "una conferenza". Sono un
  posto dove stai bene, dove possono convivere musica, lavoro, conoscenze e
  formazione per tutti sui temi più distanti, tutti uniti dall'unico obiettivo
  comune dell'accessibilità
featured_image: /assets/uploads/e6b1675c-4e37-4b6c-be21-bdd7396962c7.jpg
featured_image_alt: Logo degli accessibility days
author: Mattia
---
Quest'anno ho vissuto penso gli Accessibility days migliori di sempre! Ho condiviso con Donato, Cristian, Marco e Bianca 3 giorni stupendi nei quali abbiamo lavorato, parlato, suonato e condiviso la vita quotidiana nei quartieri romani e nell'edifico Marco Polo nel quale si sono svolti gli Accessibility days. Penso che l'immagine che più li rappresenta sia quella in cui sulle note di "We are the world" abbiamo coinvolto tutti a cantare con la neonata "Accessibility Band".

![Accessibility band (Cristian, Sauro, Donato, Mattia) che suona con alle spalle speaker, organizzatori che cantano con noi in coro](/assets/uploads/e6b1675c-4e37-4b6c-be21-bdd7396962c7.jpg)

Ho conosciuto tante persone nuove e consolidato la conoscenza con altre che ogni anno è sempre un piacere rincontrare per raccontarsi nel corso dell'anno quali progetti e avventure hanno affrontato e che obiettivi hanno raggiunto. Ho avuto il piacere di conoscere dal punto di vista personale, professionale e musicale il gruppo di SitiAccessibili col quale sono felice di lavorare perchè mi hanno dimostrato quanto credono nell'obiettivo dell'accessibilità

S﻿crivendo a circa 2 mesi dalla fine dell'evento posso dire che hanno portato e stanno portando frutto. I prossimi Accessibility days non sono poi così lontani...
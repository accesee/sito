---
title: Approvata la prima PR sul repository di Bootstrap Italia
date: 2024-09-18T14:07:02.549Z
description: Dopo aver deciso di contribuire al progetto open source di
  Designers Italia sullo sviluppo di UIKit "nazionali" per il web, condivido con
  piacere con voi la notizia che la mia prima pull request è stata approvata!
featured_image: /assets/uploads/screenshot-2024-09-18-alle-16.09.58.jpg
author: Mattia
---
N﻿on ho mai contribuito ad un progetto open source prima d'ora, ma quando mi è stato riferito che erano in corso delle valutazioni di accessibilità sui componenti sviluppati da Designers Italia non ho potuto ignorare la cosa. Non mi sarei perdonato se valutando un sito web di una pubblica amministrazione mi fossi imbattuto in un errore che avrei potuto segnalare in fase di sviluppo del progetto. Quindi eccomi qui che con Cristian testiamo componenti a 4 mani. Devo dire che è un'esperienza molto bella... Non credevo.. e siamo solo all'inizio, ci sono ancora molte issue da smarcare e PR da aprire, ma intanto sono contento delle persone con le quali ho avuto il piacere di confrontarmi finora e... alla prossima PR
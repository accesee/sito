---
title: UNIGHT - Notte dei ricercatori. 28/09/2024
date: 2024-10-23T07:55:15.691Z
description: "UNITO con Ideaginger in occasione della Notte dei Ricercatori è
  stata presente con uno stand al quale abbiamo presentato SpeechMatE: com'è ora
  e come sarà."
featured_image: /assets/uploads/img_8949.jpg
author: Mattia
---
A Torino il 28/09/2024 si è svolta la seconda giornata dell'evento UNIGHT chiamato anche "Notte dei ricercatori". In questa occasione sono andato allo stand di Ideaginger, la società che si è occupata della campagna di crowdfunding per UNITO che ha portato ad un ottimo risultato per il progetto SpeechMatE. In quella sede io e il professor Sandro Coriasco dell'Università di Torino abbiamo spiegato ai visitatori e ai curiosi il progetto facendo provare anche la vecchia versione del prototipo, ormai risalente al 2020. Oltre ogni nostra aspettativa abbiamo avuto un ottimo riscontro e abbiamo potuto vedere come persone di varie età (da bambini ad adulti) interagivano con il software di dettatura vocale della matematica (i bambini sono stati grandi). 

![Mattia che guida una ragazza seduta al tavolo all'utilizzo di speechmate](/assets/uploads/5e638a93-34a8-45d0-b0d1-458626cff09c.jpg "test della vecchia versione di speechmate")

O﻿ltre all'aspetto puramente didattico è stata un'occasione per incontrare nuove persone e per passare una bellissima giornata in compagnia di altre che per vari motivi non si ha mai abbastanza tempo di frequentare

![Mattia Ducci e Sandro Coriasco in una foto in posa all'interno di una cornice di Ideaginger](/assets/uploads/img_8953-1-.jpg "Mattia Ducci e Sandro Coriasco in posa")
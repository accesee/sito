---
color_type: default
ni_icon_name: atom
title: "Zensheet"
product_url: "https://zensheet.accesee.it"
summary: >-
    ZenSheet is a web-based platform designed to simplify the sharing and viewing of Excel files. It allows users to upload Excel files and share them via a simple link, enabling recipients to view the data without needing a ZenSheet account. The platform emphasizes accessibility, offering features such as read only sharing, anonymous access, WCAG 2.1 AA accessibility compliance, data exploration tools like sorting and filtering, and more.
description:
cover_image: /assets/img/products/zensheet-logo.png
order: 0
---
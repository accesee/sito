---
featured: true
color_type: default
ni_icon_name: atom
title: Audiofunctions 2.0
summary: >-
    Progetto guidato dal laboratorio Polin del Dipartimento di matematica dell'Università di Torino, Audiofunctions è un'applicazione web che permette l'esplorazione di funzioni matematiche da parte di non vedenti attraverso il suono.
    Il sistema è pensato per essere guidato negli sviluppi dalle esigenze didattiche e per poter essere utilizzato da 
    insegnanti in classe come strumento inclusivo per lo studio delle funzioni matematiche secondo un approccio covariazionale.
    Il sistema è supervisionato dal laboratorio Polin dell'Università degli Studi di Torino ed è sviluppato
    utilizzando il framework Javascript Vue.js con Nuxt2
description: >-
    Applicazione web che permette l'esplorazione di funzioni matematiche da parte di non vedenti attraverso il suono.
    Il sistema è pensato per essere guidato negli sviluppi dalle esigenze didattiche e per poter essere utilizzato da 
    insegnanti in classe come strumento inclusivo per lo studio delle funzioni matematiche secondo un approccio covariazionale.
    Il sistema è supervisionato dal laboratorio Polin dell'Università degli Studi di Torino ed è sviluppato
    utilizzando il framework Javascript Vue.js con Nuxt2
in_progress: true
cover_image: /assets/img/projects/af.jpg
year: 2024
---
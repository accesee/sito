---
featured: false
color_type: default
ni_icon_name: atom
title: MoleculesGateway
summary: >- 
    Piattaforma che permette di accedere ad un grande numero di molecola naturali.
    Ho avuto la fortuna di conoscere e lavorare con CodeAtlas per la manutenzione e le evolutive di questo progetto
    per la gestione di un database molecolare di <a href="https://naicons.com/">Naicons</a>.
    Si possono cercare le molecole desiderate utilizzando diversi parametri, vedere quali estratti contengono tali molecole, quali ceppi le producono e in quale quantità relativa.
    Gli utenti possono anche vedere quante molecole diverse sono presenti in un determinato estratto e confrontare la composizione di estratti diversi.
    In particolare, le molecole, gli estratti e i microrganismi produttori sono disponibili e facilmente accessibili. Per maggiori informazioni si veda
    <a href="https://micro4all.com/">il sito di presentazione di micro4all</a>. Il sistema 
    è stato sviluppato insieme a CodeAtlas con tecniche full stack usando Django, PostgreSQL e con deploy su AWS
description: >- 
    Piattaforma che permette di accedere ad un grande numero di molecola naturali. Si possono cercare le molecole desiderate utilizzando diversi parametri, vedere quali estratti contengono tali molecole, quali ceppi le producono e in quale quantità relativa.
    Gli utenti possono anche vedere quante molecole diverse sono presenti in un determinato estratto e confrontare la composizione di estratti diversi.
    In particolare, le molecole, gli estratti e i microrganismi produttori sono disponibili e facilmente accessibili. Il sistema 
    è stato sviluppato insieme a CodeAtlas con tecniche full stack usando Django, PostgreSQL e con deploy su AWS
in_progress: true
cover_image: /assets/img/projects/micro4all.png
year: 2024
---
---
title: Remediation di Presentazioni Keynote, PowerPoint e PDF
color_type: info
featured: true
ni_icon_name: atom
summary: In collaborazione con SitiAccessibili abbiamo svolto attività di
  analisi e remediation di accessibilità su presentazioni Keynote e PowerPoint
  con relativa esportazione in PDF accessibile
description: In collaborazione con SitiAccessibili ho contribuito all'analisi e
  alla remediation di documenti PowerPoint e Keynote in modo da renderli
  fruibili da parte di persone che utilizzano tecnologie assistive. In
  particolare questo lavoro ci ha permesso di toccare con mano molte casistiche
  e di creare un vero e proprio flusso di lavoro che parte dalla creazione di
  una presentazione accessibile e che termina con la creazione di un documento
  PDF accessibile. Spesso ogni passaggio presenta delle criticità dal punto di
  vista dell'accessibilità e poter lavorare su un corpus così grande di
  documenti ci ha permesso di ampliare la nostra conoscenza su molte casistiche
  e problemi pratici di chi crea e legge le presentazioni e i documenti.
in_progress: false
cover_image: /assets/uploads/logo-lavori-fst-slide.png
year: 2024
---

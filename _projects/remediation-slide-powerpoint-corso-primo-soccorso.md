---
title: Remediation slide PowerPoint corso primo soccorso
color_type: info
featured: false
ni_icon_name: atom
summary: In collaborazione con Cristian Bernareggi, abbiamo reso accessibili le
  slide delle presentazioni dei corsi di primo soccorso erogati da ManPower
  agendo sia in modo semi-automatico che manuale per rimediare i problemi di
  struttura e accessibilità delle slide. Per ogni presentazione abbiamo anche
  prodotto e validato i relativi PDF accessibili
description: In collaborazione con Cristian Bernareggi, abbiamo reso accessibili
  le slide delle presentazioni dei corsi di primo soccorso erogati da ManPower
  agendo sia in modo semi-automatico che manuale per rimediare i problemi di
  struttura e accessibilità delle slide. Per ogni presentazione abbiamo anche
  prodotto e validato i relativi PDF accessibili
in_progress: false
cover_image: /assets/uploads/manpower-a11y-ppt.jpg
order: 0
year: 2024
---

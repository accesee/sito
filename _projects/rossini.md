---
featured: true
color_type: primary
ni_icon_name: atom
title: ROSSINI
summary: >- 
    Sistema multidisciplinare per l'evacuazione dinamica di persone da impianti industriali a rischio
    di incidente rilevante NaTech. La dinamicità dell'evacuazione è data da un sistema che misura il rischio
    di incidente in tempo reale e che notifca un'app mobile iOS per l'evacuazione in grado di calcolare
    il percorso più sicuro verso un'uscita di sicurezza sulla base delle informazioni ricevute. Il sistema è stato 
    sviluppato in collaborazione con l'Università degli Studi di Milano e di Pavia che ne hanno fatto da coordinatori e sfrutta tecniche di realtà
    aumentata, SLAM (Simultaneous Localization And Mapping) e di computer vision tramite marcatori visivi (qr code) per riuscire ad avere un'accuratezza di localizzazione
    inferiore al metro. Ulteriori informazioni sono disponibili sul <a href="https://progetto-rossini.it/">sito del progetto ROSSINI</a>.
description: >- 
    Sistema multidisciplinare per l'evacuazione dinamica di persone da impianti industriali a rischio
    di incidente rilevante NaTech. La dinamicità dell'evacuazione è data da un sistema che misura il rischio
    di incidente in tempo reale e che notifca un'app mobile iOS per l'evacuazione in grado di calcolare
    il percorso più sicuro verso un'uscita di sicurezza sulla base delle informazioni ricevute. Il sistema è stato 
    sviluppato in collaborazione con l'Università degli Studi di Milano e di Pavia e sfrutta tecniche di realtà
    aumentata, SLAM (Simultaneous Localization And Mapping) e di computer vision tramite marcatori visivi (qr code) per riuscire ad avere un'accuratezza di localizzazione
    inferiore al metro.
in_progress: false
cover_image: /assets/img/projects/rossini.png
year: 2022
---
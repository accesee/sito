---
featured: true
color_type: warning
title: SI@UNIPV
ni_icon_name: atom
summary: >-
    Il sistema SI@UNIPV è 
    finalizzato a facilitare la mobilità autonoma di tutti, 
    incluse le persone con disabilità, all'interno dell'Università di Pavia e 
    favorire la scoperta del patrimonio di interesse culturale presente nell'Ateneo. Il sistema si basa
    su un'applicazione mobile iOS che rileva dei beacon bluetooth installati in punti strategici dell'Università per 
    localizzare l'utente e permettergli di raggiungere la destinazione desiderata attraverso messaggi vocali
    che tengono conto delle modalità di spostamento di persone con disabilità visiva. Il sistema è stato ideato dall'Università di Pavia
    con la quale ho collaborato per lo sviluppo. Tecnicamente il sistema utilizza Strapi come backend CMS e SwiftUI come framework di
    interfaccia utente nativo iOS. Ulteriori dettagli possono essere trovati sul <a href="https://accessibilewp.rilevant.it/">sito web del progetto RISID</a>.
description: >-
    Il sistema SI@UNIPV è 
    finalizzata a facilitare la mobilità autonoma di tutti, 
    incluse le persone con disabilità, all'interno dell'Università di Pavia e 
    favorire la scoperta del patrimonio di interesse culturale presente nell'Ateneo. Il sistema si basa
    su un'applicazione mobile iOS che rileva dei beacon bluetooth installati in punti strategici dell'Università per 
    localizzare l'utente e permettergli di raggiungere la destinazione desiderata attraverso messaggi vocali
    che tengono conto delle modalità di spostamento di persone con disabilità visiva. Il sistema è stato sviluppato
    in collaborazione con l'Università di Pavia utilizzando Strapi come backend CMS e SwiftUI come framework di
    interfaccia utente nativo iOS.

in_progress: false
cover_image: /assets/img/projects/risid.png
year: 2024
---
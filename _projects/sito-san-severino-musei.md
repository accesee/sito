---
title: Sito "San Severino Musei"
color_type: info
featured: true
ni_icon_name: atom
summary: >-
  Progettazione, design e sviluppo del sito dei musei del comune di San Severino
  Marche. Il sito serve da indice per i musei e offre la possibilità ai gestori
  dei contenuti di gestire mostre ed eventi per ciascun museo e di
  personalizzare la pagina di ciascun museo con immagini, contatti e
  informazioni generali, didattiche e di accessibilità.

  Il sito è stato sviluppato in WordPress partendo dal tema Divi4.
description: Progettazione, design e sviluppo del sito dei musei del comune di
  San Severino Marche. Il sito è stato sviluppato in WordPress partendo dal tema
  Divi4
in_progress: false
cover_image: /assets/uploads/san-severino-musei.png
order: 0
year: 2024
---

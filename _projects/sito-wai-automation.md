---
featured: false
color_type: success
ni_icon_name: atom
title: Sito web dell'azienda WAI-AUTOMATION
summary: >-
    Sito web vetrina dell'azienda di automazione industriale WAI Automation. Sviluppato con tecnologie 
    Jamstack: Jekyll + Netlify + DecapCMS + Gitlab. Con questa infrastruttura il cliente può in autonomia
    gestire i contenuti del sito da un pannello di amministrazione e non deve preoccuparsi di gestire un server.
    <a href="https://www.wai-automation.it/">Visita il sito di WAI-Automation</a>
description: >-
    Sito web vetrina dell'azienda di automazione industriale WAI Automation. Sviluppato con tecnologie 
    Jamstack: Jekyll + Netlify + DecapCMS + Gitlab. Con questa infrastruttura il cliente può in autonomia
    gestire i contenuti del sito da un pannello di amministrazione e non deve preoccuparsi di gestire un server.
in_progress: false
cover_image: /assets/img/projects/wai-automation.png
year: 2023
---
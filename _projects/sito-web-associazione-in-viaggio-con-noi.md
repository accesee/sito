---
featured: false
color_type: default
ni_icon_name: atom
title: Sito web dell'organizzazione di volontariato "In Viaggio Con Noi"
summary: >-
    Sito web dell'organizzazione di volontariato "In Viaggio Con Noi" finalizzato a promuovere le loro attività, iniziative
    e a presentare la loro associazione e la loro missione. Il sito è stato realizzato con WordPress. <a href="https://www.inviaggioconnoivolontariato.it/">Visita il sito dell'associazione</a>
description: >-
    Sito web dell'organizzazione di volontariato "In Viaggio Con Noi" finalizzato a promuovere le loro attività, iniziative
    e a presentare la loro associazione e la loro missione. Il sito è stato realizzato con WordPress
in_progress: false
cover_image: /assets/img/projects/in-viaggio-con-noi.png
year: 2021
---
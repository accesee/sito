---
title: Sonairgraph
color_type: info
featured: true
ni_icon_name: atom
summary: Col laboratorio Polin dell'Università degli Studi di Torino stiamo
  partecipando ad un progetto europeo "Erasmus+" che si pone come obiettivo
  quello della sonificazione di grafici di funzione matematici per persone con
  disabilità visive. All'interno del progetto sono presenti partner italiani ed
  europei provenienti da Germania, Repubblica Ceca, Romania, Spagna. In questo
  progetto con orizzonte triennale la sfida è partire dall'esperienza avuta con
  Audiofunctions per creare qualcosa che possa prenderne il buono, estenderlo e
  valutarlo in un orizzonte didattico che esce dai confini italiani
description: Col laboratorio Polin dell'Università degli Studi di Torino stiamo
  partecipando ad un progetto europeo "Erasmus+" che si pone come obiettivo
  quello della sonificazione di grafici di funzione matematici per persone con
  disabilità visive. All'interno del progetto sono presenti partner italiani ed
  europei provenienti da Germania, Repubblica Ceca, Romania, Spagna. In questo
  progetto con orizzonte triennale la sfida è partire dall'esperienza avuta con
  Audiofunctions per creare qualcosa che possa prenderne il buono, estenderlo e
  valutarlo in un orizzonte didattico che esce dai confini italiani
in_progress: true
cover_image: /assets/uploads/logo-progetto.png
year: 2024
---

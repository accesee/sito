---
featured: true
color_type: info
title: SpeechMatE
ni_icon_name: atom
summary: >-
    Progetto guidato dal laboratorio Polin del Dipartimento di matematica dell'Università di Torino, SpeechMatE (Speech-Driven Mathematical Editor) è un software per dettare, navigare e modificare formule 
    matematiche attraverso la sola voce. Il progetto coinvolge molti settori di ricerca di attualità come
    didattica della matematica, sviluppo software, Natural Languace Processing, Human-Computer Interaction, Large Language Models 
    e Machine Learning. Dopo un primo prototipo nel 2020 dove abbiamo dimostrato la fattibilità del progetto, siamo pronti 
    per crearne una versione più avanzata e completa che permette ad un utente di avere un vero e proprio editor matematico.
    Il progetto è supervisionato dal laboratorio Polin dell'Università degli Studi di Torino. Maggiori dettagli sono disponibili sul <a href="https://speechmate.polin.it/">sito web del progetto</a>.
description: >-
    SpeechMatE (Speech-Driven Mathematical Editor) è un software per dettare, navigare e modificare formule 
    matematiche attraverso la sola voce. Il progetto coinvolge molti settori di ricerca di attualità come
    didattica della matematica, sviluppo software, Natural Languace Processing, Human-Computer Interaction, Large Language Models 
    e Machine Learning. Dopo un primo prototipo nel 2020 dove abbiamo dimostrato la fattibilità del progetto, siamo pronti 
    per crearne una versione più avanzata e completa che permette ad un utente di avere un vero e proprio editor matematico.
    Il progetto è supervisionato dal laboratorio Polin dell'Università degli Studi di Torino. Maggiori dettagli sono disponibili sul <a href="https://speechmate.polin.it/">sito web del progetto</a>.
in_progress: true
cover_image: /assets/img/projects/speechmate.png
year: 2024
---
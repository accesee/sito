---
date: 2020-01-01
citation: "Accessibilità di contenuti digitali per le STEM  : un problema aperto. Alcune soluzioni inclusive per l’accessibilità di formule e grafici per persone con disabilità e DSA 2 2020 978-8-89-809161-4"
link: https://www.aicanet.it/documents/10776/3228919/Atti+Didamatica+2020/03982fbf-4754-402b-bf96-903b574c4a0d
keywords: [accessibilità, stem. matematica, dsa, riconoscimento vocale]
---
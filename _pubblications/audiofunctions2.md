---
date: 2024-01-01 
citation: "Carola Manolino; Margherita Piroi; Tiziana Armano; Erika Brunetto; Mattia Ducci; Silvia Funghi; Cristian Bernareggi. LA SONIFICAZIONE DEI GRAFICI DI FUNZIONE CON AUDIOFUNCTIONS 2.0, DI.FI.MA. 2023: Insegnamento e apprendimento della Matematica e della Fisica nel periodo post pandemia, ISBN: 9788875903206"
link: https://iris.unito.it/handle/2318/2039612?mode=simple
keywords: [sonificazione, stem, accessibilità, didattica della matematica]
---
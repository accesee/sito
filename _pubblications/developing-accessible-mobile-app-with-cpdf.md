---
date: 2021-10-17
citation: Sergio Mascetti, Mattia Ducci, Niccolò Cantù, Paolo Pecis, and Dragan Ahmetovic. 2021. Developing Accessible Mobile Applications with Cross-Platform Development Frameworks. In Proceedings of the 23rd International ACM SIGACCESS Conference on Computers and Accessibility (ASSETS '21). Association for Computing Machinery, New York, NY, USA, Article 35, 1–5.
link: https://doi.org/10.1145/3441852.3476469
keywords: [Human Computer Interaction, Cross platform developing frameworks, Accessibilità, Sviluppo mobile]
---
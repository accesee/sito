---
date: 2021-10-15
citation: Giorgio Presti, Dragan Ahmetovic, Mattia Ducci, Cristian Bernareggi, Luca A. Ludovico, Adriano Baratè, Federico Avanzini, and Sergio Mascetti. 2021. Iterative Design of Sonification Techniques to Support People with Visual Impairments in Obstacle Avoidance. ACM Trans. Access. Comput. 14, 4, Article 19 (December 2021), 27 pages.
link: https://doi.org/10.1145/3470649
keywords: [accessibilità, sonificazione, ostacoli, mobilità autonoma]
---
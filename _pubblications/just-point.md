---
date: 2017-10-27
citation: "Mascetti S, D'Acquisto S, Gerino A, Ducci M, Bernareggi C, Coughlan JM. JustPoint: Identifying Colors with a Natural User Interface. ASSETS. 2017 Oct-Nov;2017:329-330. doi: 10.1145/3132525.3134802. PMID: 29218331; PMCID: PMC5714614."
link: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5714614/
keywords: [Assistive Technology, Hand Recognition, Color Detection]
---
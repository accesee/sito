---
date: 2018-10-01
citation: "Niccolò Cantù, Mattia Ducci, Dragan Ahmetovic, Cristian Bernareggi, and Sergio Mascetti. 2018. MathMelodies 2: a Mobile Assistive Application for People with Visual Impairments Developed with React Native. In Proceedings of the 20th International ACM SIGACCESS Conference on Computers and Accessibility (ASSETS '18). Association for Computing Machinery, New York, NY, USA, 453–455."
link: https://doi.org/10.1145/3234695.3241006
keywords: [accessibilità, human centered design, sviluppo mobile cross platform, human computer interaction]
---
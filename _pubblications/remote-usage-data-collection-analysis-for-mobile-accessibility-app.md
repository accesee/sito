---
date: 2021-03-08
citation: "D. Ahmetovic, C. Bernareggi, M. Ducci, A. Gerino and S. Mascetti, Remote Usage Data Collection and Analysis for Mobile Accessibility Applications, 2021 IEEE International Conference on Pervasive Computing and Communications Workshops and other Affiliated Events (PerCom Workshops), Kassel, Germany, 2021, pp. 93-98, doi: 10.1109/PerComWorkshops51409.2021.9430968."
link: https://ieeexplore.ieee.org/document/9430968
keywords: [Pervasive computing, Assistive technology, Sociology, Data collection, Remote logging, mobile assistive technologies]
---
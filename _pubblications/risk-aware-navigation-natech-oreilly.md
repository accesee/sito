---
date: 2023-04-01
citation: Gerard J. O'Reilly, Davit Shahnazaryan, Paolo Dubini, Emanuele Brunesi, Annalisa Rosti, Filippo Dacarro, Alberto Gotti, Davide Silvestri, Sergio Mascetti, Mattia Ducci, Mariano Ciucci, Alessandra Marino, Risk-aware navigation in industrial plants at risk of NaTech accidents, International Journal of Disaster Risk Reduction, Volume 88, 2023, 103620, ISSN 2212-4209
link: https://www.sciencedirect.com/science/article/abs/pii/S2212420923001000?dgcid=coauthor
keywords: [natech, indoor navigation]
---
---
date: 2022-11-01
citation: O'Reilly, Gerard & Shahnazaryan, Davit & Nafeh, Al Mouayed Bellah & Ozsarac, Volkan & Sarigiannis, Denis & Dubini, Paolo & Dacarro, Filippo & Gotti, Alberto & Rosti, Annalisa & Silvestri, Davide & Brunesi, Emanuele & Mascetti, Sergio & Ducci, Mattia & Carletti, Davide & Ciucci, Mariano & Marino, Alessandra. (2022). Utilization of a Sensor Array for the Risk-Aware Navigation in Industrial Plants at Risk of NaTech Accidents. 10.1115/PVP2022-84014. 
link: https://www.researchgate.net/publication/365151402_Utilization_of_a_Sensor_Array_for_the_Risk-Aware_Navigation_in_Industrial_Plants_at_Risk_of_NaTech_Accidents
keywords: [natech, risk aware navigation]
---
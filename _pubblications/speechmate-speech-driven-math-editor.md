---
date: 2020-01-01
citation: "SpeechMatE: A Speech-driven Maths Editor for Motor-Impaired People 63 2020 978-3-9504630-2-6"
link: https://www.icchp.org/sites/default/files/ED_1_ICCHP_Forum.pdf#page=63
keywords: [speech recognition, editor, math, motor-impaired]
---
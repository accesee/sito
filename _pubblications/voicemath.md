---
date: 2024-01-01
citation: "Tiziana Armano , Gabriele Baratto, Cristian Bernareggi , Sara Bertoldo 4 , Erika Brunetto , Anna Capietto , Manuela Caramagna , Sandro Coriasco , Nazareno Defrancesco , Mattia Ducci , Maria Luisa Gabrielli , Mariafrancesca Guadalupi , Alessandro Mazzei , Angelo Sacca' , Francesco Tarasconi. Natural Language Processing e Computer Vision per l’accessibilità dei video di lezioni con formule, Conferenza GARR 2024 , Navigare la complessità Infrastrutture e competenze digitali per la ricerca, selected paper. Pagine da 18 a 23. ISBN: 978-88-946629-3-1"
link: https://iris.unito.it/handle/2318/2039590?mode=complete
keywords: [LLM, AI, NLP, STEM, accessibilità, computer vision]
---
---
date: 2019-10-01
citation: "Giorgio Presti, Dragan Ahmetovic, Mattia Ducci, Cristian Bernareggi, Luca Ludovico, Adriano Baratè, Federico Avanzini, and Sergio Mascetti. 2019. WatchOut: Obstacle Sonification for People with Visual Impairment or Blindness. In Proceedings of the 21st International ACM SIGACCESS Conference on Computers and Accessibility (ASSETS '19). Association for Computing Machinery, New York, NY, USA, 402–413."
link: https://doi.org/10.1145/3308561.3353779
keywords: [sonificazione, mobilità autonoma, ostacoli, computer vision, SLAM, sviluppo mobile, human centered design]
---
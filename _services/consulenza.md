---
ni_icon_name: "atom"
color_type: "info"
name: "Consulenza"
summary: "Cerchiamo di capire i tuoi bisogni e di trovare la soluzione migliore per te. Crediamo che il contatto diretto con il cliente e con gli utenti sia fondamentale per capire le sue esigenze e per poter offrire un servizio davvero efficace"
description: "Cerchiamo di capire i tuoi bisogni e di trovare la soluzione migliore per te. Crediamo che il contatto diretto con il cliente e con gli utenti sia fondamentale per capire le sue esigenze e per poter offrire un servizio davvero efficace"
featured_image: /assets/img/services/consulenza.jpg
aspects:
    - ni_icon_name: atom
      ni_icon_bg_color_name: default
      name: Dall'analisi al rilascio
      description: >- 
        Seguiamo i progetti dall'analisi dei requisiti al rilascio, che sia in cloud,
        sull'App Store o che sia un documento accessibile inviato via mail,
        facendo studi di fattibilità se necessario. Siamo flessibili nell'inserirci
        nel flusso di un sistema già esistente o di un progetto in corso.
      tags:
        - studio
        - processo
        - problemi
    - ni_icon_name: atom
      ni_icon_bg_color_name: default
      name: Svilluppo
      description: >-
        Siamo specializzati nello sviluppo di applicazioni web e mobile e conosciamo
        varie tecnologie e linguaggi di programmazione. Siamo quindi in grado di capire
        buona parte del codice esistente e scegliere la tecnolgia più adatta alle tue esigenze
      tags:
        - html
        - css
        - js
        - swift
        - python
        - c#
        - django
        - iOS
        - MAUI
        - Android
    - ni_icon_name: atom
      ni_icon_bg_color_name: default
      name: Studio continuo
      description: >- 
        Il mondo dell'informatica è in continua evoluzione e noi ci teniamo aggiornati
        costantemente per riuscire a stare al passo con le nuove aspettative e i nuovi 
        bisogni del mondo digitale.
      tags:
        - passione
        - competenza
        - metodo
    - ni_icon_name: atom
      ni_icon_bg_color_name: default
      name: Rapporto umano e trasparenza
      description: >-
        Noi amiamo ciò che facciamo e lavoriamo in direzione delle persone. Per questo
        anche nel lavoro ci piace avere a che fare con le persone e non con dei numeri.
        Crediamo nella trasparenza e nel fatto che la collaborazione debba essere un piacere
        per entrambe le parti.
      tags:
        - felicità
        - trasparenza
order: 0
---
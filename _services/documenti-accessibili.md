---
ni_icon_name: atom
color_type: success
name: Documenti accessibili
summary: >- 
  Creiamo e rendiamo accessibili documenti in formato digitale come PDF, Word, PowerPoint, LaTeX. 
  Forniamo anche assistenza e formazione per poterli creare in autonomia.
description: >- 
  Creiamo e rendiamo accessibili documenti in formato digitale come PDF, Word, PowerPoint, LaTeX. 
  Forniamo anche assistenza e formazione per poterli creare in autonomia.
featured_image: /assets/img/services/documenti.jpg
aspects:
    - ni_icon_name: atom
      ni_icon_bg_color_name: default
      name: Da documenti non accessibili
      description: >-
        Molti documenti, anche digitali, spesso non sono accessibili in tutto o in parte.
        Basti pensare alle scansioni di pagine cartacee, ai PDF non testuali, ai documenti Word 
        con un testo a basso contrasto o con immagini senza descrizione. Noi li analizziamo e
        capiamo come renderli accessibili.
      tags:
        - WCAG
        - PDF
        - Word
        - PowerPoint
        - analisi
    - ni_icon_name: atom
      ni_icon_bg_color_name: default
      name: Form per l'inserimento dati
      description: >-
        Un problema ricorrente e che è stato affrontato in molti modi è la creazione di form per
        l'inserimento di dati. Molti di questi non sono però accessibili. A seconda del tipo di 
        dati da raccogliere proponiamo soluzioni accessibili che vanno dalla creazione di form web a form PDF
        fino ad alternative personalizzate di raccolta dati
      tags:
        - PDF
        - web
        - soluzioni personalizzate
    - ni_icon_name: atom
      ni_icon_bg_color_name: default
      name: Trascrizioni
      description: >-
        Dai libri dati in dotazione alle scuole ai libri storici, spesso questo materiale
        esiste solo in forma cartacea o in forma digitale non accessibile e per lo stile col
        quale è stato scritto, renderlo accessibile non è la strada più conveniente o addirittura
        non è fattibile. La nostra esperienza in materia di trascrizioni di presentazioni e testi, 
        anche scientifici con formule matematiche, ci permette di offrire una soluzione accessibile
        parallela a quella che non lo è.
      tags:
        - manuale
        - latex
        - tecnologie assistive
        - conoscenza
    - ni_icon_name: atom
      ni_icon_bg_color_name: default
      name: Formazione
      description: >-
        Il modo migliore per rendere accessibile il mondo, secondo noi è formare chi lo vive.
        I contenuti e i documenti sono materiale quotidiano di tutti e conoscere come funzionano
        e come renderli accessibili ci consente di non dover intervenire ogni volta, ma di fornire
        un metodo per rendervi autonomi e consapevoli.
      tags:
        - consapevolezza
        - conoscenza
        - studio
        - ATAG
        - WCAG
order: 2
---
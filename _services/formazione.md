---
ni_icon_name: atom
color_type: primary
name: Formazione
summary: >- 
  Crediamo che l'accessibilità passi prima di tutto dalla sua conoscenza. 
  Per questo motivo, offriamo corsi di formazione per aziende, professionisti 
  che vogliono approfondire le tematiche dell'accessibilità digitale. La nostra
  conoscenza la offriamo con piacere a voi in tutti i settori che conosciamo meglio, come
  la progettazione e lo sviluppo di siti web e app accessibile e la creazione di contenuti accessibili.
description: >- 
  Crediamo che l'accessibilità passi prima di tutto dalla sua conoscenza. 
  Per questo motivo, offriamo corsi di formazione per aziende e professionisti 
  che vogliono approfondire le tematiche dell'accessibilità digitale. La nostra
  conoscenza la offriamo con piacere a voi in tutti i settori che conosciamo meglio, come
  la progettazione e lo sviluppo di siti web e app accessibile e la creazione di contenuti accessibili.
featured_image: /assets/img/services/formazione.jpg
aspects:
    - ni_icon_name: atom
      ni_icon_bg_color_name: default
      name: Accessibilità web
      description: >-
        Il web è ad oggi lo strumento che ha le maggiori potenzialità di
        accessibilità. Renderlo accessibile è molto più facile di quanto
        si possa pensare, sia dal punto di vista di sviluppo, sia da quello
        di conoscenza degli strumenti che mette già a disposizione.
      tags:
        - html
        - css
        - js
        - wcag
    - ni_icon_name: atom
      ni_icon_bg_color_name: default
      name: Creazione di documenti digitali
      description: >-
        Creare documenti è un'attività presente in ogni realtà. Saper creare
        documenti accessibili, come PDF, Word o presentazioni PowerPoint 
        può fare la differenza e risolvere problemi in modo efficace. 
        A volte semplicemente capire in quali casi è necessario 
        usare un certo formato di file può essere la soluzione.
      tags:
        - word
        - power point
        - pdf
        - latex
        - web
        - formato
    - ni_icon_name: atom
      ni_icon_bg_color_name: default
      name: Personalizzata sulla tua realtà
      description: >-
        L'accessibilità non è una checklist, ma un approccio che è possibile
        portare al di fuori del web o da un'app. Per questo motivo siamo disponibili
        a valutare insieme a voi i problemi che riscontrate nel vostro contesto
        per trovare insieme soluzioni che possano essere integrate nel vostro
        flusso di lavoro
      tags:
        - analisi
        - collaborazione
        - soluzioni alternative
        - sperimentazione
    - ni_icon_name: atom
      ni_icon_bg_color_name: default
      name: Accessibilità su dispositivi mobili
      description: >-
        Gli smartphone e i tablet sono ormai parte integrante della nostra vita
        quotidiana. Saper cosa offrono dal punto di vista dell'accessibilità
        per poterli sfruttare come strumento inclusivo può essere importante
        per scegliere una soluzione che sia allo stesso tempo accessibile e
        funzionale senza dover considerare l'acquisto di altri dispositivi.
        Per chi ha la possibilità di sviluppare applicazioni, offriamo formazione
        anche tecnica sullo sviluppo di app accessibili.
      tags:
        - swift
        - java
        - kotlin
        - cross platform
        - android
        - iOS
        - talkback
        - voiceover
order: 3
---
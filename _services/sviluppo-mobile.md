---
ni_icon_name: atom
color_type: danger
name: Sviluppo mobile
summary: >-
  Sviluppiamo applicazioni mobile per iOS e Android attraverso tecnologie native e cross-platform, 
  con un focus particolare sull'accessibilità e l'usabilità.
description: >-
  Sviluppiamo applicazioni mobile per iOS e Android attraverso tecnologie native e cross-platform, 
  con un focus particolare sull'accessibilità e l'usabilità.
featured_image: /assets/img/services/sviluppo-mobile.jpg
aspects:
    - ni_icon_name: atom
      ni_icon_bg_color_name: default
      name: Nativo
      description: >-
        In molti casi è necessario sviluppare applicazioni native per garantire 
        la massima performance e l'accesso a tutte le funzionalità specifiche del dispositivo.
        Per questo motivo abbiamo competenze nello sviluppo nativo iOS e Android con Swift, Objective-C e Java.
        Tecnologie come la realtà virtuale e aumentata, per esempio, fanno uso di funzionalità native che è
        complesso, se non impossibile, utilizzare con tecnologie cross-platform. Utilizziamo ambienti di sviluppo nativi
        come Android Studio e Xcode per implementare e rilasciare le nostre applicazioni.
      tags:
        - ios
        - android
        - xcode
        - android studio
    - ni_icon_name: atom
      ni_icon_bg_color_name: default
      name: Cross platform
      description: >- 
        Scrivere un'applicazione una volta sola e poterla eseguire su più piattaforme è un vantaggio, soprattutto se
        le funzionalità di cui abbiamo bisogno sono comuni a tutte le piattaforme. Per questo motivo utilizziamo tecnologie
        cross-platform come .NET MAUI (Xamarin.Forms) per sviluppare applicazioni che possono essere eseguite su iOS e Android con una
        singola codebase. Questo ci permette di ridurre i tempi di sviluppo e di manutenzione garantendo un'esperienza
        molto simile a quella nativa. A seconda delle esigenze, data l'offerta di framework cross-platform, scegliamo
        il framework più adatto al progetto.
      tags:
        - ios
        - android
        - .net maui
        - xamarin.forms
        - visual studio
    - ni_icon_name: atom
      ni_icon_bg_color_name: default
      name: Accessibile
      description: >- 
        Valutare la natura dell'applicazione per scegliere il tipo di sviluppo, le tecnologie e le librerie da utilizzare
        è un lavoro che dev'essere fatto già in fase di analisi. In particolare, se l'applicazione deve essere accessibile
        non tutte le funzionalità native sono presenti in tutti i framework. Quindi è importante valutare se il framework
        o la tecnologia nativa supporta ciò di cui abbiamo davvero bisogno. Agire a valle porta quasi sempre a soluzioni
        di compromesso che non soddisfano appieno le esigenze dell'utente. 
      tags:
        - ios
        - android
        - .net maui
        - xamarin.forms
        - visual studio
    - ni_icon_name: atom
      ni_icon_bg_color_name: default
      name: Deploy
      description: >- 
        Lo scopo dello sviluppo è il rilascio dell'applicazione. Oltre allo sviluppo offriamo soluzioni di rilascio
        e pubblicazione delle applicazioni su Google Play e App Store 
      tags:
        - Google Play
        - App Store
order: 5
---
---
ni_icon_name: atom
color_type: default
name: Sviluppo web
summary: >-
  Creiamo siti web, applicazioni web personalizzate e servizi API, cercando di usare la tecnologia migliore
   a seconda delle esigenze. Le soluzioni spaziano quindi da siti web statici e full stack
   ad applicazioni web complesse che facciano leva su componenti server ed API.
description: >-
  Creiamo siti web, applicazioni web personalizzate e servizi API, cercando di usare la tecnologia migliore
   a seconda delle esigenze. Le soluzioni spaziano quindi da siti web statici e full stack
   ad applicazioni web complesse che facciano leva su componenti server ed API.
featured_image: /assets/img/services/sviluppo-web.jpg
aspects:
    - ni_icon_name: atom
      ni_icon_bg_color_name: default
      name: Jamstack
      description: >-
        Lo sviluppo Jamstack è una metodologia di sviluppo web moderna che permette di creare siti web veloci, sicuri
        che spesso sono chiamati anche statici perchè non richiedono server per essere visualizzati. Arrivano infatti al browser
        dell'utente già pronti e si possono interfacciare con servizi esterni per recuperare dati in tempo reale.
        Nel caso più semplice un sito statico è composto solo da file HTML, CSS e Javascript, il che ne semplifica la gestione
        e la manutenzione. Nonostante questo è possibile avere un sito SEO friendly e che permetta l'aggiornamento dei contenuti
        in modo semplice e veloce con vari CMS.
      tags:
        - jekyll
        - netlify
        - nuxt
        - api
        - headless
        - cms
        - seo
    - ni_icon_name: atom
      ni_icon_bg_color_name: default
      name: Full Stack
      description: >-
        In molti casi i progetti web sono più complessi di un semplice sito web e richiedono un server che si occupa
        di autenticare e gestire le richieste degli utenti e di fornire i dati necessari interrogando database, file
        o altri servizi online. In questi casi si parla di sviluppo full stack perchè si sviluppa sia il front end sia il 
        back end. Questo permette di creare applicazioni web complesse che possono essere scalate e personalizzate.
      tags:
        - django
        - rails
        - server
    - ni_icon_name: atom
      ni_icon_bg_color_name: default
      name: API
      description: >-
        Le API sono interfacce che permettono a due sistemi di comunicare tra loro. Non hanno in genere una componente grafica
        o front end, ma sono metaforicamente dei servizi che vengono interrogati per ottenere dati o eseguire operazioni.
        I servizi API sono interrogate da siti web statici o applicazioni full stack e permettono di implementare un'architettura
        a microservizi che permette di scalare e personalizzare i servizi in modo indipendente.
      tags:
        - servizi
        - microservizi
        - cloud
        - json
        - comunicazione
    - ni_icon_name: atom
      ni_icon_bg_color_name: default
      name: Wordpress
      description: >-
        Sviluppare da zero un sito web può essere un'operazione complessa e costosa. Per questo spesso si ricorre a CMS come
        Wordpress che permettono di creare siti web rapidamente e con costi contenuti. Wordpress è un CMS molto flessibile
        e che sta dietro alla maggior parte dei siti web mondiali. Negli anni è stato arricchito con plugin e temi che permettono
        di trovare soluzioni per molte esigenze e che rendono facile la gestione dei contenuti, la SEO e la personalizzazione.
        Wordpress ha anche un impegno particolare per l'accessibilità e la sicurezza ed è possibile trovare temi e plugin che rispettano
        questi requisiti. Ha lo svantaggio di essere poco o difficilmente personalizzabile al di fuori dell'interfaccia messa a disposizione
        dal template, per cui spesso è più conveniente ricorrere a soluzioni Jamstack o Full Stack
      tags:
        - cms
        - temi
        - template
        - accessibilità
        - sicurezza
order: 4
---
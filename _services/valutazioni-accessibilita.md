---
ni_icon_name: atom
color_type: info
name: Valutazioni di accessibilità
summary: >- 
  Valutiamo l'accessibilità del tuo sito web e della tua applicazione mobile in conformità alle linee guida WCAG. 
  Ti garantiamo di svolgere test automatici e manuali e consegnandoti un report dettagliato
  sulle criticità e le possibili soluzioni per migliorarlo
description: >- 
  Valutiamo l'accessibilità del tuo sito web e della tua applicazione mobile in conformità alle linee guida WCAG. 
  Ti garantiamo di svolgere test automatici e manuali e consegnandoti un report dettagliato
  sulle criticità e le possibili soluzioni per migliorarlo
featured_image: /assets/img/services/valutazione-accessibilita.jpg
aspects:
    - ni_icon_name: atom
      ni_icon_bg_color_name: default
      name: Applicazioni mobile
      description: >-
        Valutiamo l'accessibilità della tua applicazione mobile iOS e Android in conformità alle linee guida WCAG.
        Testiamo l'applicazione con strumenti automatici di valutazione di accessibilità specifici e con test manuali.
        Se necessario consideriamo anche un protocollo di test con utenti con disabilità potenziali utilizzatori dell'app.
        Ti proponiamo soluzioni personalizzate per ciascuna piattaforma redigendo relazioni 
        e ti possiamo aiutare a implementarle.
      tags:
        - iOS
        - android
        - strumenti di accessibilità
        - wcag
        - analisi
        - test
    - ni_icon_name: atom
      ni_icon_bg_color_name: default
      name: Siti e applicazioni web
      description: >-
        Valutiamo l'accessibilità del tuo sito web in conformità alle linee guida WCAG.
        Testiamo il sito con strumenti automatici di valutazione di accessibilità e con test manuali.
        Nel mondo web, più ancora che in quello per dispositivi mobili, non mancano plugin di browser
        e strumenti di valutazione di accessibilità.
        Se necessario consideriamo anche un protocollo di test con utenti con disabilità potenziali utilizzatori del sito.
        Ti proponiamo soluzioni personalizzate redigendo relazioni e ti possiamo aiutare a implementarle.
      tags:
        - chrome
        - browser
        - screen reader
        - wcag
    - ni_icon_name: atom
      ni_icon_bg_color_name: default
      name: Test automatizzati
      description: >-
        Nel processo di valutazione di accessibilità del tuo sito web o della tua applicazione mobile i testi automatici
        svolgono un ruolo fondamentale nel trovare le criticità più comuni, come un errato contrasto tra elementi grafici
        o immagini senza un testo alternativo. Utilizziamo strumenti di valutazione di accessibilità specifici per il web
        che analizzano vari aspetti dell'accessibilità e che ci possono suggerire punti in cui intervenire manualmente.
      tags:
        - valutazione
        - wcag
        - estensioni
        - report
    - ni_icon_name: atom
      ni_icon_bg_color_name: default
      name: Test manuali e con gli utenti
      description: >-
        Dopo aver svolto i test automatici è fondamentale svolgere test manuali per verificare la correttezza delle valutazioni.
        Infatti, per trovare alcune criticità è necessario svolgere test manuali, come ad esempio la correttezza di un testo 
        alternativo di un'immagine o la correttezza di un'etichetta di un campo di input. In generale: i test automatizzati
        possono verificare la presenza o meno di un certo requisito, ma non possono valutarne la qualità, processo per il quale
        è necessario l'intervento umano.
      tags:
        - design
        - human centered design
        - semantica
order: 1
---
---
title: Corso accessibilità dirigenti regione Marche (PNRR Misura 1.4.2 Citizen
  Inclusion)
color_type: info
ni_icon_name: atom
summary: In collaborazione con <a
  href="https://grifomultimedia.it/en/home-en/">GrifoMultimedia</a> e
  nell'ambito della misura 1.4.2 del PNRR Citizen Inclusion, ho erogato ai
  dirigenti della regione Marche un corso sull'accessibilità volto a fornire
  loro le basi per comprendere le varie disabilità e le relative tecnologie
  assistive. Durante il corso sono anche state spiegate le norme e gli standard
  internazionali, europei ed italiani che regolano l'accessibilità digitale.
  Infine è stato spiegato come redigere documenti accessibili con software di
  videoscrittura. Del corso sono state erogate 2 edizioni
end_date: 2025-01-24T07:34:14.408Z
delivery: ibrida
hours: 9
target: dirigenti
---
Il corso diretto ai dirigenti della regione Marche ha visto lo svolgersi di 2 edizioni di 9 ore ciascuna: 4 ore via webinar e 5 ore di laboratorio in presenza presso regione Marche. Gli argomenti trattati sono stati i seguenti:

- Disabilità sensoriali, motorie, cognitive e relative tecnologie assistive

- Contesto normativo:
	- 	Riferimenti internazionali: WCAG, UNI CEI EN 301549, Convenzione delle Nazioni Unite sui diritti delle persone con disabilità (UNCRPD), ISO/IEC 71/2014, sezioni 504 e 508 americane (Rehabilitation Act, Telecommunication Act e Assistive Technology Act)
	- Riferimenti europei: direttive UE 2016/2102 e UE 2019/882 (European Accessibility Act)
	- Riferimenti italiani: Legge 04/2004, Codice dell'Amministrazione Digitale, DL 222/2023, Piano Triennale per l'informatica nella PA, UNI EN ISO 9999:2017 in merito di tecnologie assistive disponibili per postazioni di lavoro per persone con disabilità
	- Linee guida AGID
	- PIAO

- Produzione di documenti accessibili in Microsoft Word e LibreOffice
- Cenni sulla valutazione di accessibilità di un sito web (automatica e soggettiva) e relativo monitoraggio AGID di applicazioni mobile, siti e documenti

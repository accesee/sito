---
color_type: default
ni_icon_name: atom
title: Creazione e sviluppo di documenti accessibili, WCAG e ARIA base e avanzato per siti web e applicazioni mobili
summary: 'In collaborazione con <a href="https://sitiaccessibili.it/">SitiAccessibili</a> abbiamo erogato ai tecnici di TIG Factory un corso approfondito sulla creazione e lo sviluppo di siti web, applicazioni mobili e documenti accessibili secondo le linee guida WCAG 2.1 AA'
target: tecnici
end_date: 2024-11-05
delivery: online
hours: 10
---

Il corso è stato erogato in modalità online e ha avuto una durata complessiva di 10 ore. Durante il corso sono stati affrontati i seguenti argomenti:
- Introduzione sull'accessibilità e alla produzione di documenti accessibili
- Introduzione alle linee guida WCAG 2.1 AA
- Introduzione alle linee guida ARIA
- Casi avanzati di utilizzo di ARIA
- Creazione di documenti accessibili con Word
- Introduzione allo standard PDF/UA, alla sua produzione, verifica e remediation sia maualmente che per via programmatica
- Analisi di casi studio specifici di TIG sia per la parte documentale che per la parte web
- Sviluppo di documenti PDF accessibili
- Introduzione allo sviluppo accessibile per dispositivi mobili
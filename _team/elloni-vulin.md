---
full_name: Elloni Vulin
order: 2
short_bio: Architetto e interior designer appassionata di grafica
key_points:
    - name: Architettura
      ni_icon_name: atom
      ni_icon_bg_color_name: info
    - name: Design
      ni_icon_name: atom
      ni_icon_bg_color_name: info
    - name: Grafica
      ni_icon_name: atom
      ni_icon_bg_color_name: info
    - name: Sportiva e Twirler
      ni_icon_name: atom
      ni_icon_bg_color_name: info
socials:
    - name: instagram
      link: https://www.instagram.com/elloni_v/
profile_image:
    filename: /assets/img/team/elloni-vulin.jpg
    alt_text: "Elloni in posa mezzo busto sorridente sulla spiaggia in riva al mare al tramonto"

---
⁠Laureata in architettura al Politecnico di Milano nel 2019, 
mi occupo di progettazione e ristrutturazione in ambiti residenziali, 
direzionali e ricettivi. Per me architettura significa progettare spazi belli, 
accessibili e adatti a tutte le esigenze. Io li definisco “spazi sensoriali”: 
realizzare spazi da vivere e per generare nuovi ricordi. 
Al mio lavoro ho sempre affiancato la grafica in quanto la 
reputo uno strumento di comunicazione molto importante. Nel 
tempo libero mi piace leggere, ascoltare musica, viaggiare, fare pilates e twirling.
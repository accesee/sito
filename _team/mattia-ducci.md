---
full_name: Mattia Ducci
order: 1
short_bio: >- 
  Sviluppatore freelance web e mobile appassionato di accessibilità e di musica
key_points:
    - name: Curioso e sempre studente
      ni_icon_name: atom
      ni_icon_bg_color_name: info
    - name: Sonificazione 
      ni_icon_name: atom
      ni_icon_bg_color_name: info
    - name: Applicazioni mobili 
      ni_icon_name: atom
      ni_icon_bg_color_name: info
    - name: Siti web
      ni_icon_name: atom
      ni_icon_bg_color_name: info
    - name: Documenti accessibili
      ni_icon_name: atom
      ni_icon_bg_color_name: info
    - name: Accessibilità STEM
      ni_icon_name: atom
      ni_icon_bg_color_name: info
socials:
    - name: linkedin
      link: https://www.linkedin.com/in/mattia-ducci-dev/
    - name: twitter
      link: https://twitter.com/Mattia541993
profile_image:
    filename: /assets/img/team/mattia-ducci.jpg
    alt_text: "Mattia sorridente e in posa seduto ad un tavolo con un computer portatile davanti a lui"
cv_link: /assets/pdfs/cvs/mattia-ducci.pdf

---

## La mia esperienza lavorativa in breve
Sono uno sviluppatore web e mobile appassionato di accessibilità. 
Ho conseguito la laurea in informatica nel 2019 all'Università degli 
Studi di Milano dopo una laurea triennale in informatica musicale. 
Dall'inizio della mia laurea magistrale ho collaborato con le Università degli 
Studi di Milano, Torino e Pavia su progetti sia legati all'accessibilità che su altre 
tematiche spaziando dalle trascrizioni in LaTeX di libri di matematica a sviluppo 
di applicazioni web e mobile di supporto alla mobilità autonoma, alla 
localizzazione e allo studio di materie scientifiche (STEM). 
Nel frattempo ho lavorato per oltre 5 anni come sviluppatore web e mobile in una software house
inserito nei team di sviluppo mobile e web nei quali ho conosciuto lo sviluppo iOS,
cross-platform e web dai siti statici alle applicazioni full stack per clienti pubblici e privati
di vari settori economici e produttivi. Nell'ultimo periodo mi occupo molto di tematiche legate alla formazione,
valutazioni di accessibilità di siti web e app continuando le mie attività di sviluppo web e mobile su consulenza e progetti personali.

### Formazione
- Laurea triennale in Informatica musicale presso l'Università degli studi di Milano
- Laurea magistrale in Informatica presso l'Università degli studi di Milano

### Competenze accessibilità digitale
- Dal 2018 collaboro a progetti di ricerca con le Università di Milano, Torino e Pavia su progetti legati alla mobilità autonoma e didattica inclusiva nelle fasi di progettazione e sviluppo software
- Dal 2017 sono relatore e correlatore di interventi a conferenze e webinar nell'ambito dell'accessibilità digitale e della didattica inclusiva quali ad esempio: Accessibility days e SeMa.
- Dal 2022 tra le mie attività mi occupo di valutazioni di accessibilità di siti web e applicazioni mobile
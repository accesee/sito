---
full_name: Nicolò Faranda
order: 3
short_bio: Software Engineer Intern. Scrivetemi a <a href="mailto:nicolfaranda@outlook.it">nicolfaranda@outlook.it</a>
key_points:
    - name: Problem solving
      ni_icon_name: atom
      ni_icon_bg_color_name: default
    - name: Pensiero critico
      ni_icon_name: atom
      ni_icon_bg_color_name: default
    - name: Esplorazione metodi
      ni_icon_name: atom
      ni_icon_bg_color_name: default
    - name: Innovazione
      ni_icon_name: atom
      ni_icon_bg_color_name: default
    - name: Versatilità
      ni_icon_name: atom
      ni_icon_bg_color_name: default
    - name: Creatività
      ni_icon_name: atom
      ni_icon_bg_color_name: default
socials:
    - name:
      link: 
    - name:
      link:
    - name:
      link:
profile_image:
    filename: /assets/img/team/nicolo-faranda.jpeg
    alt_text: Immagine placeholder in stile polygon art con tonalità dal nero al rosa

---
Sono Nicolò, una persona riflessiva e in costante evoluzione, sempre alla ricerca di 
nuove opportunità per ampliare le mie conoscenze e competenze. 
Attualmente sto portando a termine il mio percorso di studi presso il Politecnico di 
Milano e sto consolidando la mia esperienza attraverso la collaborazione con il team, 
dove ho avuto l'opportunità di esplorare diversi metodi e punti di vista, arricchendomi 
così di una prospettiva più ampia e inclusiva. La mia personalità versatile e aperta mi 
rende incline a lavorare bene in gruppo, valorizzando la diversità di idee e contributi.
Mi piace affrontare le sfide con un approccio flessibile e disponibile, sempre pronto a 
imparare dalle esperienze e a crescere sia personalmente che professionalmente. Sono 
entusiasta dell'opportunità di fare parte dell’ambiente di lavoro di Accesee e sono 
motivato a continuare questo percorso di scoperta e sviluppo insieme a voi.

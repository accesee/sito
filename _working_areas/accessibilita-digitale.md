---
ni_icon_name: atom
color_type: default
name: Accessibilità digitale
description: >-
    Il bisogno di accessibilità si trova in molti contesti e settori. In alcuni casi
    non si ha il tempo o la necessità di entrare in profondità in una realtà, ma
    se ne scalfisce solo la superficie conoscendo chi ci lavora, come e a cosa una persona
    specializzata in soluzioni accessibili può essergli utile. Mi sento quindi di aggiungere
    questo ambito generale di esperienza perchè è uno di quelli che rende stupendo il nostro lavoro
order: 0
---
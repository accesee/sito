---
ni_icon_name: atom
color_type: warning
name: Didattica per le STEM
description: >-
    Con progetti che spaziano dalla trascrizione in LaTeX di documenti scientifici non accessibili
    alla creazione di soluzioni inclusive per l'apprendimento della matematica per studenti
    di varie età e disabilità.
order: 4
---
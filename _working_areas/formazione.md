---
ni_icon_name: atom
color_type: warning
name: Formazione e divulgazione
description: >-
    La formazione e la divulgazione sono aspetti fondamentali al fine di creare una cultura dell'accessibilità.
    Tra le nostre attività la formazione ha quindi un'importanza speciale e l'applichiamo a contesti privati e pubblici su vari temi legati all'accessibilità: dalle basi all'adattamento personalizzato al flusso di lavoro e ai materiali dell'azienda
order: 1
---
---
ni_icon_name: atom
color_type: success
name: Industria NaTech e automazione
description: >- 
    Con un progetto interdisciplinare durato due anni finalizzato alla creazione di un sistema
    di navigazione dinamico per l'evacuazione da un impianto industriale di persone in caso di incidente
    a rischio rilevante e la collaborazione con un'azienda nel settore automazione abbiamo sviluppato una conoscenza di dominio sui bisogni e sul flusso di lavoro in queste realtà
order: 6
---
---
ni_icon_name: atom
color_type: primary
name: Medicina del lavoro
description: >-    
    Con esperienza pluriennale di sviluppo e manutenzione di software iOS e web
    a supporto delle attività dei medici del lavoro.
order: 5
---
---
ni_icon_name: atom
color_type: info
name: Mobilità autonoma
description: >- 
    Con progetti di sviluppo principalmente su dispositivi mobili per persone con disabilità visiva
    abbiamo fatto molta esperienza con varie tecnologie di navigazione e localizzazione indoor e outdoor
    e sperimentato molte interfacce e tecniche di comunicazione, anche innovative
order: 2
---
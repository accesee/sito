---
ni_icon_name: atom
color_type: danger
name: Ricerca
description: >-
    Noi veniamo dal mondo universitario e della ricerca. 
    L'accessibilità è un tema aperto e in continua evoluzione anche all'interno della comunità
    scientifica. Noi stessi siamo impegnati in progetti di ricerca, sperimentazione e pubblicazione
order: 3
---